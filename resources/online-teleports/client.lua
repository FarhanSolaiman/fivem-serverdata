key_to_teleport = 38

positions = {
	-- -- Nightclub
	-- {{-22.500, 214.920, 105.550, 168.010}, {-1637.610, -2989.780, -78.600, 261.020}, "Press E to Teleport | Nightclub Garage"},
	-- {{4.519, 220.326, 106.600, 248.940}, {-1569.374, -3017.177, -76.000, 0.329}, "Press E to Teleport | Nightclub"},
	-- -- Houses
	-- {{-174.901, 502.377, 136.400, 77.733}, {-174.339, 497.536, 136.060, 190.776}, "Press E to Teleport | 3655 - Wild Oats Dr."},
    -- {{-167.593, 478.521, 132.300, 344.395}, {-167.332, 476.792, 132.700, 189.395}, "Press E to Teleport | Deck"},
	-- {{347.110, 440.860, 146.700, 295.325}, {341.877, 437.765, 147.850, 113.547}, "Press E to Teleport | 2044 - North Conker Ave"},
	-- {{373.731, 427.791, 144.700, 54.982}, {373.590, 423.721, 144.320, 165.157}, "Press E to Teleport | 2045 - North Conker Ave"},
	-- {{119.320, 564.700, 182.900, 359.680}, {117.196, 559.920, 182.700, 178.512}, "Press E to Teleport | 3677 - Whispymound Dr."},
	-- {{-1294.250, 454.830, 96.350, 4.800}, {-1289.871, 449.691, 96.400, 178.826}, "Press E to Teleport | 2113 - Mad Wayne Thunder Dr."},
	-- {{-853.060, 696.050, 147.800, 8.950}, {-859.906, 691.430, 151.400, 178.617}, "Press E to Teleport | 2874 - Hillcrest Ave"},
	-- {{-753.173, 620.436, 141.500, 285.513}, {-758.364, 619.071, 142.600, 107.752}, "Press E to Teleport | 2868 - Hillcrest Ave"},
	-- {{-686.490, 596.840, 142.600, 39.001}, {-682.433, 592.444, 143.900, 220.426}, "Press E to Teleport | 2862 - Hillcrest Ave"},
	-- -- Clubhouses
 	-- {{2471.757, 4110.815, 36.900, 66.281}, {997.256, -3158.109, -40.500, 268.836}, "Press E to Teleport | Grapeseed Biker Clubhouse"},
 	-- {{2466.457, 4101.402, 36.900, 81.567}, {998.828, -3164.342, -40.150, 266.851}, "Press E to Teleport | Grapeseed Biker Clubhouse Garage"},
	-- {{-379.840, 6062.490, 30.450, 46.500}, {1121.050, -3152.130, -38.600, 355.170}, "Press E to Teleport | Paleto Bay Biker Clubhouse"},
 	-- {{-355.334, 6067.529, 30.450, 45.678}, {1110.144, -3164.258, -38.700, 356.001}, "Press E to Teleport | Paleto Bay Biker Clubhouse Garage"},
	-- Warehouses
	-- {{-253.018, -2591.167, 5.000, 80.300}, {1088.666, -3187.666, -40.500, 177.669}, "Press ~INPUT_PICKUP~ to Enter/Exit | Cocaine Warehouse"},
	-- {{-235.385, -2574.540, 4.000, 357.135}, {1103.323, -3195.890, -40.500, 89.395}, "Press E to Enter/Exit | Cocaine Warehouse Garage"},
	-- {{-1170.993, -1380.938, 3.900, 30.094}, {1138.213, -3198.802, -41.500, 357.076}, "Press ~INPUT_PICKUP~ to Enter/Exit | Counterfeit Cash Factory"},
	-- {{-1168.950, -1388.900, 3.900, 131.874}, {1118.727, -3193.275, -42.000, 177.346}, "Press E to Enter/Exit | Counterfeit Cash Factory Garage"},
	-- {{1643.770, 4857.890, 41.000, 96.51}, {1173.509, -3196.669, -40.500, 88.377}, "Press E to Enter/Exit | Document Forgery Office"},
	-- {{1181.454, -3113.752, 5.000, 85.741}, {997.490, -3200.700, -37.450, 274.344}, "Press ~INPUT_PICKUP~ to Enter/Exit | Methamphetamine Lab"},
    -- {{102.171, 175.514, 103.600, 168.486}, {1066.010, -3183.380, -40.700, 93.012}, "Press ~INPUT_PICKUP~ to Enter/Exit | Weed Farm"},
    -- Pillbox Hospital
    {{340.55, -595.37, 27.92, 64.5}, {330.02, -601.15, 42.36, 67.5}, "Press ~INPUT_PICKUP~ to Enter/Exit | Upper/Lower Pillbox Hospital"},
    {{332.06, -595.62, 42.36, 67.5}, {338.67, -583.92, 73.19, 251.5}, "Press ~INPUT_PICKUP~ to Enter/Exit | Helipad"},
    -- Mafia Mansion
    {{-334.93, 1114.59, 329.56, 202.85}, {-335.72, 1114.18, 324.51, 198.33}, "Press ~INPUT_PICKUP~ to Enter/Exit | 4th/3rd floor"},
    {{-331.38, 1115.51, 324.51, 191.33}, {-335.1, 1114.77, 319.46, 198.11}, "Press ~INPUT_PICKUP~ to Enter/Exit | 3rd/2nd floor"},
    {{-331.38, 1115.51, 319.51, 191.33}, {-335.1, 1114.77, 314.46, 198.11}, "Press ~INPUT_PICKUP~ to Enter/Exit | 2nd/1st floor"},
    {{-331.38, 1115.51, 314.51, 191.33}, {-335.1, 1114.77, 309.46, 198.11}, "Press ~INPUT_PICKUP~ to Enter/Exit | 1st/Ground floor"},
    {{-338.72, 1104.19, 309.36, 287.33}, {-338.72, 1104.19, 304.06, 287.33}, "Press ~INPUT_PICKUP~ to Enter/Exit | Ground/Armory floor"},
    {{-321.97, 1109.75, 309.36, 107.73}, {-321.97, 1109.75, 301.17, 107.73}, "Press ~INPUT_PICKUP~ to Enter/Exit | Ground/Armory floor"},
    -- Misc
	-- {{1.171, -702.283, 15.000, 339.267}, {10.073, -667.206, 32.450, 8.440}, "Press E to Teleport | Union Depository"},
}

-----------------------------------------------------------------------------
-------------------------DO NOT EDIT BELOW THIS LINE-------------------------
-----------------------------------------------------------------------------

local player = GetPlayerPed(-1)

Citizen.CreateThread(function ()
    while true do
        Citizen.Wait(5)
        local player = GetPlayerPed(-1)
        local playerLoc = GetEntityCoords(player)

        for i,location in ipairs(positions) do
            teleport_text = location[3]
            loc1 = {
                x=location[1][1],
                y=location[1][2],
                z=location[1][3],
                heading=location[1][4]
            }
            loc2 = {
                x=location[2][1],
                y=location[2][2],
                z=location[2][3],
                heading=location[2][4]
            }

			-- DrawMarker(1, loc1.x, loc1.y, loc1.z, 0, 0, 0, 0, 0, 0, 1.0, 1.0, 0.5, 255, 255, 255, 200, 0, 0, 0, 0)
            -- DrawMarker(1, loc2.x, loc2.y, loc2.z, 0, 0, 0, 0, 0, 0, 1.0, 1.0, 0.5, 255, 255, 255, 200, 0, 0, 0, 0)

            if CheckPos(playerLoc.x, playerLoc.y, playerLoc.z, loc1.x, loc1.y, loc1.z, 2) then 
                alert(teleport_text)
                
                if IsControlJustReleased(1, key_to_teleport) then
                    if IsPedInAnyVehicle(player, true) then
                        SetEntityCoords(GetVehiclePedIsUsing(player), loc2.x, loc2.y, loc2.z)
                        SetEntityHeading(GetVehiclePedIsUsing(player), loc2.heading)
                    else
                        SetEntityCoords(player, loc2.x, loc2.y, loc2.z)
                        SetEntityHeading(player, loc2.heading)
                    end
                end

            elseif CheckPos(playerLoc.x, playerLoc.y, playerLoc.z, loc2.x, loc2.y, loc2.z, 2) then
                alert(teleport_text)

                if IsControlJustReleased(1, key_to_teleport) then
                    if IsPedInAnyVehicle(player, true) then
                        SetEntityCoords(GetVehiclePedIsUsing(player), loc1.x, loc1.y, loc1.z)
                        SetEntityHeading(GetVehiclePedIsUsing(player), loc1.heading)
                    else
                        SetEntityCoords(player, loc1.x, loc1.y, loc1.z)
                        SetEntityHeading(player, loc1.heading)
                    end
                end
            end            
        end
    end
end)

function CheckPos(x, y, z, cx, cy, cz, radius)
    local t1 = x - cx
    local t12 = t1^2

    local t2 = y-cy
    local t21 = t2^2

    local t3 = z - cz
    local t31 = t3^2

    return (t12 + t21 + t31) <= radius^2
end

function alert(msg)
    SetTextComponentFormat("STRING")
    AddTextComponentString(msg)
    DisplayHelpTextFromStringLabel(0,0,1,-1)
end