local beds = {
    { x = 358.68, y = -571.29, z = 28.8, h = 342.5, taken = false, model = 2117668672 },
    { x = 355.3, y = -569.86, z = 28.8, h = 342.5, taken = false, model = 2117668672 },
    { x = 351.92, y = -568.69, z = 28.8, h = 342.5, taken = false, model = 2117668672 },
    { x = 354.15, y = -562.06, z = 28.8, h = 156.5, taken = false, model = 2117668672 },
    { x = 357.6, y = -563.4, z = 28.8, h = 156.5, taken = false, model = 2117668672 },
    { x = 360.95, y = -564.75, z = 28.8, h = 156.5, taken = false, model = 2117668672 },
}

local beds2 = {
    { x = 322.76, y = -587.08, z = 43.2, h = 338.81, taken = false, model = 2117668672 },
    { x = 317.76, y = -585.22, z = 43.2, h = 338.81, taken = false, model = 2117668672 },
    { x = 314.64, y = -584.02, z = 43.2, h = 338.81, taken = false, model = 2117668672 },
    { x = 311.13, y = -592.88, z = 43.2, h = 338.81, taken = false, model = 2117668672 },
    { x = 307.80, y = -581.49, z = 43.2, h = 338.81, taken = false, model = 2117668672 },
    { x = 309.16, y = -577.53, z = 43.2, h = 164.52, taken = false, model = 2117668672 },
    { x = 313.78, y = -579.21, z = 43.2, h = 164.52, taken = false, model = 2117668672 },
    { x = 319.25, y = -581.24, z = 43.2, h = 164.52, taken = false, model = 2117668672 },
    { x = 324.26, y = -582.92, z = 43.2, h = 164.52, taken = false, model = 2117668672 },
}

local bedsTaken = {}
local injuryBasePrice = 7000
ESX             = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

AddEventHandler('playerDropped', function()
    if bedsTaken[source] ~= nil then
        beds[bedsTaken[source]].taken = false
    end
end)

AddEventHandler('playerDropped', function()
    if bedsTaken[source] ~= nil then
        beds[bedsTaken[source]].taken = false
    end
end)

RegisterServerEvent('mythic_hospital:server:RequestBed')
AddEventHandler('mythic_hospital:server:RequestBed', function()
    for k, v in pairs(beds) do
        if not v.taken then
            v.taken = true
            bedsTaken[source] = k
            TriggerClientEvent('mythic_hospital:client:SendToBed', source, k, v)
            return
        end
    end

    TriggerClientEvent('mythic_notify:client:SendAlert', source, { type = 'error', text = 'No beds available.' })
end)

RegisterServerEvent('mythic_hospital:server:RequestBed2')
AddEventHandler('mythic_hospital:server:RequestBed2', function()
    for k, v in pairs(beds2) do
        if not v.taken then
            v.taken = true
            bedsTaken[source] = k
            TriggerClientEvent('mythic_hospital:client:SendToBed', source, k, v)
            return
        end
    end

    TriggerClientEvent('mythic_notify:client:SendAlert', source, { type = 'error', text = 'No beds available.' })
end)

RegisterServerEvent('mythic_hospital:server:RPRequestBed')
AddEventHandler('mythic_hospital:server:RPRequestBed', function(plyCoords)
    local foundbed = false
    for k, v in pairs(beds) do
        local distance = #(vector3(v.x, v.y, v.z) - plyCoords)
        if distance < 3.0 then
            if not v.taken then
                v.taken = true
                foundbed = true
                TriggerClientEvent('mythic_hospital:client:RPSendToBed', source, k, v)
                return
            else
                TriggerClientEvent('mythic_notify:client:SendAlert', source, { type = 'error', text = 'That bed is taken.' })
            end
        end
    end

    if not foundbed then
        TriggerClientEvent('mythic_notify:client:SendAlert', source, { type = 'inform', text = 'You are not near a hospital bed.' })
    end
end)

RegisterServerEvent('mythic_hospital:server:EnteredBed')
AddEventHandler('mythic_hospital:server:EnteredBed', function()
    local src = source
    local injuries = GetCharsInjuries(src)

    local totalBill = injuryBasePrice

    if injuries ~= nil then
        for k, v in pairs(injuries.limbs) do
            if v.isDamaged then
                totalBill = totalBill + (injuryBasePrice * v.severity)
            end
        end

        if injuries.isBleeding > 0 then
            totalBill = totalBill + (injuryBasePrice * injuries.isBleeding)
        end
    end

    -- print(source)
    -- YOU NEED TO IMPLEMENT YOUR FRAMEWORKS BILLING HERE
    TriggerEvent('esx_billing:sendBill', src, 'society_ambulance', 'Hospital Check-in', totalBill)
	-- local xPlayer = ESX.GetPlayerFromId(src)
    -- xPlayer.removeBank(totalBill)
    TriggerClientEvent('mythic_notify:client:SendAlert', source, { type = 'success', text = 'You were billed for $' .. totalBill ..'.' })
    TriggerClientEvent('mythic_hospital:client:FinishServices', src)
end)

RegisterServerEvent('mythic_hospital:server:LeaveBed')
AddEventHandler('mythic_hospital:server:LeaveBed', function(id)
    beds[id].taken = false
end)
