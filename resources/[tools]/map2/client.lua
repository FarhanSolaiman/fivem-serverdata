Citizen.CreateThread(function()
    SetMapZoomDataLevel(0, 0.96, 0.9, 0.08, 0.0, 0.0)
    SetMapZoomDataLevel(1, 1.6, 0.9, 0.08, 0.0, 0.0)
    SetMapZoomDataLevel(2, 8.6, 0.9, 0.08, 0.0, 0.0)
    SetMapZoomDataLevel(3, 12.3, 0.9, 0.08, 0.0, 0.0)
    SetMapZoomDataLevel(4, 22.3, 0.9, 0.08, 0.0, 0.0)
    SetMapZoomDataLevel('GOLF_COURSE', 55.0, 0., 0.1, 2.0, 1.0)
    SetMapZoomDataLevel('INTERIOR', 450.0, 0.0, 0.1, 1.0, 1.0)
    SetMapZoomDataLevel('GALLERY', 4.5, 0.0, 0.0, 0.0, 0.0)
    SetMapZoomDataLevel('GALLERY_MAXIMIZE', 11.0, 0.0, 0.0, 2.0, 3.0)
end)

Citizen.CreateThread(function()
    while true do
		Citizen.Wait(1)
		if IsPedOnFoot(GetPlayerPed(-1)) then 
			SetRadarZoom(1100)
		elseif IsPedInAnyVehicle(GetPlayerPed(-1), true) then
			SetRadarZoom(1100)
		end
    end
end)