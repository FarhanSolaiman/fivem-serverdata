Config = {}

Config.AllLogs = true											-- Enable/Disable All Logs Channel
Config.postal = false  											-- set to false if you want to disable nerest postal (https://forum.cfx.re/t/release-postal-code-map-minimap-new-improved-v1-2/147458)
Config.username = "ADRENALINE RP SYSTEM" 							-- Bot Username
Config.avatar = "http://hanngbayan.ddns.net:30120/webadmin/icon.png"				-- Bot Avatar
Config.communtiyName = "ADRENALINE RP"					-- Icon top of the Embed
Config.communtiyLogo = "http://hanngbayan.ddns.net:30120/webadmin/icon.png"		-- Icon top of the Embed


Config.weaponLog = false  			-- set to false to disable the shooting weapon logs
Config.weaponLogDelay = 1000		-- delay to wait after someone fired a weapon to check again in ms (put to 0 to disable) Best to keep this at atleast 1000

Config.playerID = true				-- set to false to disable Player ID in the logs
Config.steamID = true				-- set to false to disable Steam ID in the logs
Config.steamURL = true				-- set to false to disable Steam URL in the logs
Config.discordID = true				-- set to false to disable Discord ID in the logs


-- Change color of the default embeds here
-- It used Decimal color codes witch you can get and convert here: https://jokedevil.com/colorPicker
Config.joinColor = "3863105" 		-- Player Connecting
Config.leaveColor = "15874618"		-- Player Disconnected
Config.chatColor = "10592673"		-- Chat Message
Config.shootingColor = "10373"		-- Shooting a weapon
Config.deathColor = "000000"		-- Player Died
Config.resourceColor = "15461951"	-- Resource Stopped/Started



Config.webhooks = {
	all = "",
	chat = "https://discordapp.com/api/webhooks/736122595195224155/h8eBjUpDn2BDSVQzXLuNSk7KWll1lBwQlcQ4dNpKpieY_uT-JUeRJ3uhjZlxqkj3ePLq",
	joins = "https://discordapp.com/api/webhooks/736122014024335412/dSVx6Lgmrxw-rG8_4t34jsL2hPNnJN3Pit3vHzZ97BgyTrFBibJuRbT9SQkaV7t7hD6n",
	leaving = "https://discordapp.com/api/webhooks/736122014024335412/dSVx6Lgmrxw-rG8_4t34jsL2hPNnJN3Pit3vHzZ97BgyTrFBibJuRbT9SQkaV7t7hD6n",
	deaths = "https://discordapp.com/api/webhooks/736122159369289810/QkXrdwSogUN2YuNGQ5N-cmb1qbBSuAct-LTAhqN9a5Mha-IEaYivgz1vqmNFpxGy_KS4",
	shooting = "",
	resources = "https://discordapp.com/api/webhooks/736206788906450980/NaU7Grf4xcZJvfT3eyQzm9gOHQg3r0einJrO1bCNNXSKzPx4x8LGI5jD83MIIr9MhHH9",

  -- How you add more logs is explained on https://docs.jokedevil.com/JD_logs
  }


 --Debug shizzels :D
Config.debug = false
Config.versionCheck = "1.1.0"
