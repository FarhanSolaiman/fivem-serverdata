Config = {}
Config.EatPrice = 79
Config.DrinkPrice = 50

Config.Zones = {
    { x = 436.47, y = -986.42, z = 30.68 },
    { x = 309.07, y = -585.64, z = 43.28 },
    { x = 350.61, y = -584.61, z = 28.79 },
    { x = -208.09, y = -1341.81, z = 34.89 },
}