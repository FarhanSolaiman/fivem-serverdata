local toggleKit = false 
local disablekeys = false 
local medkit = nil
local droppedmedkit = nil

--[[
MEDKIT TOGGLE
]]--

RegisterCommand("medbag", function(source, args, raw)
    toggleKit = not toggleKit
    TriggerEvent( 'fivemedical:togmedkit' )
end, false)

RegisterNetEvent("fivemedical:togmedkit")
AddEventHandler("fivemedical:togmedkit", function()

    if toggleKit then
	
        if medkit == nil then
			medkit = CreateObject(GetHashKey("prop_cs_shopping_bag"), 0, 0, 0, true, true, true) -- creates object
			disablekeys = true
        end		
		
        AttachEntityToEntity(medkit, GetPlayerPed(-1), GetPedBoneIndex(GetPlayerPed(-1), 57005), 0.4, 0, 0, 0, 270.0, 60.0, true, true, false, true, 1, true) -- object is attached to right hand
        drawNotification("You have grabbed your medical kit!")
        
    else
        if medkit ~= nil then
            DeleteEntity(medkit) -- deletes medkit
			disablekeys = false
            medkit = nil
        end

        drawNotification("You have stowed your medical kit!")   	
			
    end
end)

--[[
END OF MEDKIT TOGGLE
]]--

--[[
MEDKIT DROP
]]--

RegisterCommand("dropmedbag", function(source, args, raw)
    TriggerEvent( 'fivemedical:dropmedkit' )
end, false)

RegisterNetEvent("fivemedical:dropmedkit")
AddEventHandler("fivemedical:dropmedkit", function()

    if ( toggleKit == true ) then
		
		if ( medkit ~= nil ) then   	
			DeleteEntity(medkit)    
			disablekeys = false        
			medkit = nil

			x, y, z = table.unpack(GetEntityCoords(GetPlayerPed(-1), true))				
			droppedmedkit = CreateObject("prop_cs_shopping_bag", x, y, z, false, true, true)		
		end

		PlaceObjectOnGroundProperly(droppedmedkit)
		drawNotification("You have placed your medical kit on the ground and unzipped it!")
		
		else
			if medkit ~= nil then
				drawNotification("You don't have your medical kit on you!")
				drawNotification("Type /togglemedkit to toggle it!")
		end
    end
end)

--[[
END OF MEDKIT DROP
]]--

--[[
MEDKIT PICKUP (after drop)
]]--

RegisterCommand("pickupmedbag", function(source, args, raw)
    TriggerEvent( 'fivemedical:pickupmedkit' )
end, false)

RegisterNetEvent("fivemedical:pickupmedkit")
AddEventHandler("fivemedical:pickupmedkit", function()

    if ( droppedmedkit ~= nil ) and ( medkit == nil ) then
			
			DeleteEntity(droppedmedkit)    
			droppedmedkit = nil
			
			drawNotification("You have picked up your medical kit!")
			
			toggleKit = not toggleKit
			TriggerEvent( 'fivemedical:togmedkit' )

	else
	    if ( droppedmedkit == nil ) and ( medkit ~= nil ) then
			drawNotification("Your medical kit is not on the ground!")
			drawNotification("Type /dropmedkit to drop it!")
			
        end
    end
end)

 













--[[
MICELLANEOUS
]]--

function drawNotification(text)
    SetNotificationTextEntry("STRING")
    AddTextComponentString(text)
    DrawNotification(false, false)
end

Citizen.CreateThread(function()
	while true do
		Citizen.Wait( 0 )
			if ( disablekeys == true ) then
				DisableControlAction(0,21,true) -- disable sprint
				DisableControlAction(0,22,true) -- disable jump				
				DisableControlAction(0,24,true) -- disable attack
				DisableControlAction(0,25,true) -- disable aim
				DisableControlAction(0,47,true) -- disable weapon
				DisableControlAction(0,58,true) -- disable weapon						
				DisableControlAction(0,263,true) -- disable melee
				DisableControlAction(0,264,true) -- disable melee
				DisableControlAction(0,257,true) -- disable melee
				DisableControlAction(0,140,true) -- disable melee
				DisableControlAction(0,141,true) -- disable melee
				DisableControlAction(0,142,true) -- disable melee
				DisableControlAction(0,143,true) -- disable melee        
				DisableControlAction(0,158,true) -- disable 2
				DisableControlAction(0,159,true) -- disable 6
				DisableControlAction(0,160,true) -- disable 3
				DisableControlAction(0,161,true) -- disable 7
				DisableControlAction(0,162,true) -- disable 8
				DisableControlAction(0,163,true) -- disable 9
				DisableControlAction(0,164,true) -- disable 4
				DisableControlAction(0,165,true) -- disable 5

			elseif ( disablekeys == false ) then
	  
		end
	end
end)

--[[
END OF MICELLANEOUS
]]--