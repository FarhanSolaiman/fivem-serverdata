ESX = nil
TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

local timers = { -- if you want more job shifts add table entry here same as the examples below
    ambulance = {
        {} -- don't edit inside
    },
    police = {
        {} -- don't edit inside
    },
    mechanic = {
        {} -- don't edit inside
    },
    taxi = {
        {} -- don't edit inside
    }
    -- fbi = {}
}
local dcname = "Adrenaline City Job Shift Logger" -- bot's name
local http = "https://discordapp.com/api/webhooks/741375552233144350/0zMcH6ZXYN7EZPOZeeiPGSzCdTkhjnZEpA0elmu35nFh60-uBBO1SI2DVbiRYukhY-6l" -- webhook for police
local http2 = "https://discordapp.com/api/webhooks/741375227497283714/CwGoVJ3emAv9qm6s1pHauGpZNUffnB7j4Kk7FisaPrHwtVvsRqN1BUQbroL4yg-fUZa3" -- webhook for ems (you can add as many as you want)
local http3 = "https://discordapp.com/api/webhooks/741375409915953233/8asKlcklj6BKHrnAqesdLs9IMKNinnB3PTmlIfDEoivK6MoXX3-E1Sf30OVqg1IXZ9nR" -- webhook for mechanic
local http4 = "" -- webhook for taxi
local avatar = "http://hanngbayan.ddns.net:30120/webadmin/icon.png" -- bot's avatar

function DiscordLog(name, message, color, job)
    local connect = {
        {
            ["color"] = color,
            ["title"] = "**".. name .."**",
            ["description"] = message,
            ["footer"] = {
                ["text"] = "Adrenaline City",
            },
        }
    }
    if job == "police" then
        PerformHttpRequest(http, function(err, text, headers) end, 'POST', json.encode({username = dcname, embeds = connect, avatar_url = avatar}), { ['Content-Type'] = 'application/json' })
    elseif job == "ambulance" then
        PerformHttpRequest(http2, function(err, text, headers) end, 'POST', json.encode({username = dcname, embeds = connect, avatar_url = avatar}), { ['Content-Type'] = 'application/json' })
    elseif job == "mechanic" then
        PerformHttpRequest(http3, function(err, text, headers) end, 'POST', json.encode({username = dcname, embeds = connect, avatar_url = avatar}), { ['Content-Type'] = 'application/json' })
    elseif job == "taxi" then
        PerformHttpRequest(http4, function(err, text, headers) end, 'POST', json.encode({username = dcname, embeds = connect, avatar_url = avatar}), { ['Content-Type'] = 'application/json' })
    end
end

RegisterServerEvent("utk_sl:userjoined")
AddEventHandler("utk_sl:userjoined", function(job)
    local id = source
    local xPlayer = ESX.GetPlayerFromId(id)

    table.insert(timers[job], {id = xPlayer.source, identifier = xPlayer.identifier, position = xPlayer.job.grade_label, name = xPlayer.name, time = os.time(), date = os.date("%d/%m/%Y %X",os.time())})
end)

RegisterServerEvent("utk_sl:jobchanged")
AddEventHandler("utk_sl:jobchanged", function(old, new, method)
    local xPlayer = ESX.GetPlayerFromId(source)
    local header = nil
    local color = nil

    if old == "police" then
        header = "Police Shift" -- Header
        color = 3447003 -- Color
    elseif old == "ambulance" then
        header = "EMS Shift"
        color = 16711680
    elseif old == "mechanic" then
        header = "Mechanic Shift"
        color = 16753920
    elseif old == "taxi" then
        header = "Taxi Shift"
        color = 16776960
    --elseif job == "fbi" then
        --header = "FBI Shift"
        --color = 3447003
    end
    if method == 1 then
        for i = 1, #timers[old], 1 do
            if timers[old][i].identifier == xPlayer.identifier then
                local duration = os.time() - timers[old][i].time
                local date = timers[old][i].date
                local timetext = nil

                if duration > 0 and duration < 60 then
                    timetext = tostring(math.floor(duration)).." seconds"
                elseif duration >= 60 and duration < 3600 then
                    timetext = tostring(math.floor(duration / 60)).." minutes"
                elseif duration >= 3600 then
                    timetext = tostring(math.floor(duration / 3600).." hours, "..tostring(math.floor(math.fmod(duration, 3600)) / 60)).." minutes"
                end

                DiscordLog(header, "Steam Name: **"..timers[old][i].name.."**\nPosition: **"..timers[old][i].position.."**\nIdentifier: **"..timers[old][i].identifier.."**\n Shift duration: **__"..timetext.."__**\n Start date: **"..date.."**\n End date: **"..os.date("%d/%m/%Y %X", os.time()).."**", color, old)
                table.remove(timers[old], i)
                break
            end
        end
    end
    if not (timers[new] == nil) then
        for t, l in pairs(timers[new]) do
            if l.id == xPlayer.source then
                table.remove(table[new], l)
            end
        end
    end
    if new == "police" or new == "ambulance" or new == "mechanic" or new == "taxi" then
        table.insert(timers[new], {id = xPlayer.source, identifier = xPlayer.identifier, position = xPlayer.job.grade_label, name = xPlayer.name, time = os.time(), date = os.date("%d/%m/%Y %X",os.time())})
    end
end)

AddEventHandler("playerDropped", function(reason)
    local id = source
    local header = nil
    local color = nil

    for k, v in pairs(timers) do
        for n = 1, #timers[k], 1 do
            if timers[k][n].id == id then
                local duration = os.time() - timers[k][n].time
                local date = timers[k][n].date
                local timetext = nil

                if k == "police" then
                    header = "Police Shift" -- Header
                    color = 3447003 -- Color
                elseif k == "ambulance" then
                    header = "EMS Shift"
                    color = 16711680
                elseif k == "mechanic" then
                    header = "Mechanic Shift"
                    color = 16753920
                elseif k == "taxi" then
                    header = "Taxi Shift"
                    color = 16776960
                end
                if duration > 0 and duration < 60 then
                    timetext = tostring(math.floor(duration)).." seconds"
                elseif duration >= 60 and duration < 3600 then
                    timetext = tostring(math.floor(duration / 60)).." minutes"
                elseif duration >= 3600 then
                    timetext = tostring(math.floor(duration / 3600).." hours, "..tostring(math.floor(math.fmod(duration, 3600)) / 60)).." minutes"
                end
                DiscordLog(header, "Steam Name: **"..timers[k][n].name.."**\nPosition: **"..timers[k][n].position.."**\nIdentifier: **"..timers[k][n].identifier.."**\n Shift duration: **__"..timetext.."__**\n Start date: **"..date.."**\n End date: **"..os.date("%d/%m/%Y %X").."**", color, k)
                table.remove(timers[k], n)
                return
            end
        end
    end
end)

-- DiscordLog("[utk_shiftlog]", "Shift logger started!", 3447003, "police")