Locales['en'] = {
  
  ['ooc_prefix'] = '^0[^3OOC - LOCAL^0] ^5@%s^0',
  ['global_ooc_prefix'] = '[OOC] %s',
  ['twt_help'] = 'Send a message as a tweet. This message will appear globally as a tweet.',
  ['twt_prefix'] = '^0[^4Twitter^0] (^5@%s^0)',
  ['me_help'] = 'describes an action that a character in the game is unable to demonstrate / perform',
  ['me_prefix'] = '[ME] %s',
  ['do_help'] = 'serves to provoke interaction with another player',
  ['do_prefix'] = '[DO] %s',
  ['bm_prefix'] = 'sends a message to Blackmarket.',
  ['generic_argument_name' ] = 'a message',
  ['generic_argument_help'] = 'a message',
  ['unknown_command'] = '^3%s^0 is not a valid command!',
}
