ESX = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

AddEventHandler('es:invalidCommandHandler', function(source, command_args, user)
  CancelEvent()
  TriggerClientEvent('chat:addMessage', source, {
      template = '<div style="padding: 0.45vw; margin: 0.05vw; background-color: rgba(205, 0, 0, 0.6); border-radius: 10px;"><i class="fas fa-exclamation"></i>  This is not a valid command</i></div>',
      args = {}
    })
end)


RegisterServerEvent('sendmetoall')
AddEventHandler('sendmetoall', function(message)
  local _source = source
  local name = GetCharacterName(_source)
  xPlayer = ESX.GetPlayerFromId(_source)

  TriggerClientEvent('esx_rpchat:sendProximityMessage', -1, xPlayer.source, name, message, { 186, 0, 255 })
  TriggerClientEvent('3dme:triggerDisplay', -1, message, xPlayer.source)
end)


-- GPS command
RegisterCommand('gps', function(source, args, raw)
 TriggerClientEvent('esx_rpchat:getCoords', source, source);
end)

RegisterServerEvent('esx_rpchat:showCoord')
AddEventHandler('esx_rpchat:showCoord', function(source, msg)
  TriggerClientEvent('chat:addMessage', source, {
     template = '<div style="padding: 0.45vw; margin: 0.05vw; background-color: rgba(0, 90, 90, 0.6); border-radius: 10px;"><i class="fas fa-map-marker-alt"></i> GPS: {0}</div>',
        args = { msg }
    })
end)

AddEventHandler('chatMessage', function(source, name, message)
  if string.sub(message, 1, string.len('/')) ~= '/' then
    CancelEvent()

    if Config.EnableESXIdentity then name = GetCharacterName(source) end

  --   TriggerClientEvent('esx_rpchat:sendLocalOOC', -1, source, name, message, {30, 144, 255});
  end
end)

-- ME command
TriggerEvent('es:addCommand', 'me', function(source, args, rawCommand)
  if source == 0 then
    print('esx_rpchat: you can\'t use this command from rcon!')
    return
  end

  args = table.concat(args, ' ')
  local name = GetPlayerName(source)
  if Config.EnableESXIdentity then name = GetCharacterName(source) end

  -- TriggerClientEvent('esx_rpchat:sendMe', -1, source, name, args, { 196, 33, 246 })
  TriggerClientEvent('3dme:triggerDisplay', -1, args, source)
  --print(('%s: %s'):format(name, args))
end)

-- DO command
TriggerEvent('es:addCommand', 'do', function(source, args, rawCommand)
  if source == 0 then
    print('esx_rpchat: you can\'t use this command from rcon!')
    return
  end

  args = table.concat(args, ' ')
  local name = GetPlayerName(source)
  if Config.EnableESXIdentity then name = GetCharacterName(source) end

  -- TriggerClientEvent('esx_rpchat:sendDo', -1, source, name, args, { 255, 198, 0 })
  TriggerClientEvent('3ddo:triggerDisplay', -1, args, source)
  --print(('%s: %s'):format(name, args))
end)

TriggerEvent('es:addCommand', 'doc', function(source, args, rawCommand)
  if source == 0 then
    print('esx_rpchat: you can\'t use this command from rcon!')
    return
  end
  if args == nil then
  print('source .. args .. rawCommand')
  return
  end
  args = table.concat(args, ' ')
  local name = GetPlayerName(source)
  if Config.EnableESXIdentity then name = GetCharacterName(source) end
  local counter_doc = 0
  local pocetOpakovani = tonumber(args)
  if pocetOpakovani < 101 then
    while counter_doc < pocetOpakovani do
        counter_doc = counter_doc + 1 
        -- TriggerClientEvent('esx_rpchat:sendDo', -1, source, name, counter_doc .. "/" .. pocetOpakovani , { 255, 198, 0 })
        -- TriggerClientEvent('3ddo:triggerDisplay', -1, args, source)
        TriggerClientEvent('3ddoa:triggerDisplay', -1, counter_doc .. "/" .. pocetOpakovani, source)
        Citizen.Wait(2000)
    end 
  end
end)

-- TriggerEvent("es:addGroupCommand", "announce", "admin", function(source, args, rawCommand)
--   local playerName = GetPlayerName(source)
--  --  local msg = rawCommand:sub(6)
--     local toSay = ''
--        for i=1,#args do
--     toSay = toSay .. args[i] .. ' ' -- Concats two strings together
--   end  

--   TriggerClientEvent('chat:addMessage', -1, {
--       template = '<div style="padding: 0.45vw; margin: 0.05vw; background-color: rgba(204, 0, 0, 0.9); border-radius: 10px;"><i class="fas fa-bullhorn"></i>Notification: {0}</div>',
--       args = { toSay }
--   })


-- end, false)
-- TWEET
-- RegisterCommand('tweet', function(source, args, rawCommand)
--     local playerName = GetPlayerName(source)
--     local msg = rawCommand:sub(6)
--     fal = GetCharacterName(source)

--     TriggerClientEvent('chat:addMessage', -1, {
--         template = '<div style="padding: 0.5vw; margin: 0.5vw; background-color: rgba(28, 160, 242, 0.6); border-radius: 10px;"><i class="fab fa-twitter"></i> @{0}:<br> {1}</div>',
--         args = { fal, msg }
--     })
-- end, false)--]]

-- BLACKMARKET
-- TriggerEvent('es:addCommand', 'bm', function(source, args, rawCommand)
--     local playerName = GetPlayerName(source)
-- --   local msg = rawCommand:sub(3)
--     -- fal = GetCharacterName(source)
--     local xPlayer = ESX.GetPlayerFromId(source)
--     local toSay = ''
--        for i=1,#args do
--     toSay = toSay .. args[i] .. ' ' -- Concats two strings together
--   end

--   if xPlayer.job.name ~= 'police' then
--     TriggerClientEvent('chat:addMessage', -1, {
--       template = '<div style="padding: 0.3vw 0.8vw; margin: 0.5vw 0.5vw 0.5vw 0; border-radius:10px; background-color: rgba(202,40,40, 0.6);"><strong style="font-size: 10pt;"><img src="https://stars.djmetla.eu/blackmarket.png" width="20" height="20" style="position: relative; left: -5px; top:3px;">Blackmarket message:</strong><br><p style="padding-top: .3vw">{0}</p></div>',
--         args = {toSay}
--     })
--   end
-- end, false)--]]

-- NEWS
-- TriggerEvent('es:addCommand', 'news', function(source, args, rawCommand)
--     local playerName = GetPlayerName(source)
--  --  local msg = rawCommand:sub(6)
--     local toSay = ''
--        for i=1,#args do
--     toSay = toSay .. args[i] .. ' ' -- Concats two strings together
--   end

--     job = GetCharacterJobName(source)


--     if job == 'lifeinvader' then 
--     TriggerClientEvent('chat:addMessage', -1, {
--       template = '<div style="padding: 0.45vw; margin: 0.05vw; background-color: rgba(205, 0, 0, 0.9); border-radius: 10px;"><i class="fas fa-newspaper"></i>  Weazel News: {0}</div>',
--         args = {toSay}
--     })
--   else 
--     TriggerClientEvent('chat:addMessage', source, {
--       template = '<div style="padding: 0.45vw; margin: 0.05vw; background-color: rgba(205, 0, 0, 0.9); border-radius: 10px;"><i class="fas fa-exclamation"></i>  You have to work for Weazel News to be able to use it /news <i class="fas fa-exclamation"></i></div>',
--       args = {}
--     })
--   end
-- end, false)

-- POLICE
TriggerEvent('es:addCommand', 'acpd', function(source, args, rawCommand)
    local playerName = GetCharacterName(source)
--    local msg = rawCommand:sub(7)
    local xPlayer = ESX.GetPlayerFromId(source)
    local toSay = ''
       for i=1,#args do
    toSay = toSay .. args[i] .. ' ' -- Concats two strings together
  end

    if xPlayer.job.name == 'police' and xPlayer.job.grade_name ~= 'undercover' then 
    TriggerClientEvent('chat:addMessage', -1, {
        template = '<div style="padding: 0.45vw; margin: 0.05vw; background-color: rgba(11, 97, 164, 0.7); border-radius: 10px; word-wrap: break-word;"><i class="fas fa-bullhorn"></i> ACPD '.. xPlayer.job.grade_label .. ' '.. playerName ..': {0}</div>',
          args = {toSay}
      })
    elseif xPlayer.job.grade_name == 'undercover' then 
    TriggerClientEvent('chat:addMessage', -1, {
        template = '<div style="padding: 0.45vw; margin: 0.05vw; background-color: rgba(11, 97, 164, 0.7); border-radius: 10px; word-wrap: break-word;"><i class="fas fa-bullhorn"></i> ACPD Captain '.. playerName ..': {0}</div>',
          args = {toSay}
      })
    else 
      TriggerClientEvent('chat:addMessage', source, {
        template = '<div style="padding: 0.45vw; margin: 0.05vw; background-color: rgba(205, 0, 0, 0.9); border-radius: 10px; word-wrap: break-word;"><i class="fas fa-exclamation"></i> You have to work for the police to be able to apply /acpd! <i class="fas fa-exclamation"></i></div>',
        args = {}
      })
    end
end, false)

-- AMBULANCE
TriggerEvent('es:addCommand', 'ach', function(source, args, rawCommand)
    local playerName = GetCharacterName(source)
--    local msg = rawCommand:sub(4)
    local xPlayer = ESX.GetPlayerFromId(source)
    local toSay = ''
       for i=1,#args do
    toSay = toSay .. args[i] .. ' ' -- Concats two strings together
  end

    if xPlayer.job.name == 'ambulance' then 
    TriggerClientEvent('chat:addMessage', -1, {
        template = '<div style="padding: 0.45vw; margin: 0.05vw; background-color: rgba(197, 0, 0, 0.7); border-radius: 10px; word-wrap: break-word;"><i class="🆘"></i> 🆘ACH '.. xPlayer.job.grade_label .. ' '.. playerName ..': {0}</div>',
          args = {toSay}
      })
  else 
    TriggerClientEvent('chat:addMessage', source, {
      template = '<div style="padding: 0.45vw; margin: 0.05vw; background-color: rgba(205, 0, 0, 0.9); border-radius: 10px; word-wrap: break-word;"><i class="fas fa-exclamation"></i> You need to work in a hospital to be able to use /ach! <i class="fas fa-exclamation"></i></div>',
      args = {}
    })
  end
end, false)

-- OOC
TriggerEvent('es:addCommand', 'ooc', function(source, args, rawCommand)
  local playerName = GetCharacterName(source)
--    local msg = rawCommand:sub(4)
  local xPlayer = ESX.GetPlayerFromId(source)
  local toSay = ''
     for i=1,#args do
  toSay = toSay .. args[i] .. ' ' -- Concats two strings together
end

  TriggerClientEvent('chat:addMessage', -1, {
      template = '<div style="padding: 0.45vw; margin: 0.05vw; background-color: rgba(255, 73, 0, 0.7); border-radius: 10px; word-wrap: break-word;"><i class="🆘"></i>🙍 [OOC] '.. xPlayer.name ..': {0}</div>',
        args = {toSay}
    })
end, false)

-- Staff
TriggerEvent('es:addGroupCommand', 'staff', "mod", function(source, args, user)
	local playerName = GetCharacterName(source)
--    local msg = rawCommand:sub(4)
  local xPlayer = ESX.GetPlayerFromId(source)
  local toSay = ''
     for i=1,#args do
  toSay = toSay .. args[i] .. ' ' -- Concats two strings together
end

  TriggerClientEvent('chat:addMessage', -1, {
      template = '<div style="padding: 0.45vw; margin: 0.05vw; background-color: rgba(1, 147, 154, 0.7); border-radius: 10px; word-wrap: break-word;"><i class="🆘"></i>👁️ [Staff] '.. xPlayer.name ..': {0}</div>',
        args = {toSay}
    })
end, function(source, args, user)
	TriggerClientEvent('chat:addMessage', source, {
    template = '<div style="padding: 0.45vw; margin: 0.05vw; background-color: rgba(205, 0, 0, 0.9); border-radius: 10px; word-wrap: break-word;"><i class="fas fa-exclamation"></i> You are not part of the staff team. <i class="fas fa-exclamation"></i></div>',
    args = {}
  })
end, {help = "Used for staff announcements", params = {{name = "message", help = "A message"}}})


-- BAZAR
-- TriggerEvent('es:addCommand', 'cardealer', function(source, args, rawCommand)
--     local playerName = GetPlayerName(source)
-- --    local msg = rawCommand:sub(6)
--     local xPlayer = ESX.GetPlayerFromId(source)
--     local toSay = ''
--        for i=1,#args do
--     toSay = toSay .. args[i] .. ' ' -- Concats two strings together
--   end

--     if xPlayer.job.name == 'cardealer' then 
--     TriggerClientEvent('chat:addMessage', -1, {
--         template = '<div style="padding: 0.45vw; margin: 0.05vw; background-color: rgba(23, 138, 0, 0.9); border-radius: 10px;"><i class="🆘"></i>🏎 Cardealer: {0}</div>',
--           args = {toSay}
--       })
--   else 
--     TriggerClientEvent('chat:addMessage', source, {
--       template = '<div style="padding: 0.45vw; margin: 0.05vw; background-color: rgba(205, 0, 0, 0.9); border-radius: 10px;"><i class="fas fa-exclamation"></i> You have to work at a cardealer to be able to use it /cardealer <i class="fas fa-exclamation"></i></div>',
--       args = {}
--     })
--   end
-- end, false)

-- MECHANIC
TriggerEvent('es:addCommand', 'acmg', function(source, args, rawCommand)
    local playerName = GetCharacterName(source)
--    local msg = rawCommand:sub(4)
    local xPlayer = ESX.GetPlayerFromId(source)
    local toSay = ''
       for i=1,#args do
    toSay = toSay .. args[i] .. ' ' -- Concats two strings together
  end

    if xPlayer.job.name == 'mechanic' then 
    TriggerClientEvent('chat:addMessage', -1, {
        template = '<div style="padding: 0.45vw; margin: 0.05vw; background-color: rgba(0, 201, 13, 0.7); border-radius: 10px; word-wrap: break-word;"><i class="🆘"></i>🔧 ACMG '.. xPlayer.job.grade_label .. ' '.. playerName ..': {0}</div>',
          args = {toSay}
      })
  else 
    TriggerClientEvent('chat:addMessage', source, {
      template = '<div style="padding: 0.45vw; margin: 0.05vw; background-color: rgba(205, 0, 0, 0.9); border-radius: 10px; word-wrap: break-word;"><i class="fas fa-exclamation"></i> You need to work as a mechanic for the ability to use /acmg! <i class="fas fa-exclamation"></i></div>',
      args = {}
    })
  end
end, false)

-- Taxi
TriggerEvent('es:addCommand', 'acuber', function(source, args, rawCommand)
  local playerName = GetCharacterName(source)
--    local msg = rawCommand:sub(4)
  local xPlayer = ESX.GetPlayerFromId(source)
  local toSay = ''
     for i=1,#args do
  toSay = toSay .. args[i] .. ' ' -- Concats two strings together
end

  if xPlayer.job.name == 'taxi' then 
  TriggerClientEvent('chat:addMessage', -1, {
      template = '<div style="padding: 0.45vw; margin: 0.05vw; background-color: rgb(0,0,0,0.7); border-radius: 10px; word-wrap: break-word;"><i class="🆘"></i>🚘 Uber '.. xPlayer.job.grade_label .. ' '.. playerName ..': {0}</div>',
        args = {toSay}
    })
else 
  TriggerClientEvent('chat:addMessage', source, {
    template = '<div style="padding: 0.45vw; margin: 0.05vw; background-color: rgba(205, 0, 0, 0.9); border-radius: 10px; word-wrap: break-word;"><i class="fas fa-exclamation"></i> You need to work as an uber driver for the ability to use /acuber! <i class="fas fa-exclamation"></i></div>',
    args = {}
  })
end
end, false)

-- Surenos
TriggerEvent('es:addCommand', 'slf', function(source, args, rawCommand)
  local playerName = GetCharacterName(source)
--    local msg = rawCommand:sub(4)
  local xPlayer = ESX.GetPlayerFromId(source)
  local toSay = ''
     for i=1,#args do
  toSay = toSay .. args[i] .. ' ' -- Concats two strings together
end

  if xPlayer.job.name == 'surenos' then 
  TriggerClientEvent('chat:addMessage', -1, {
      template = '<div style="padding: 0.45vw; margin: 0.05vw; background-color: rgb(0,0,0,0.7); border-radius: 10px; word-wrap: break-word;"><i class="🆘"></i>🕴️ Sureños la Familia '.. xPlayer.job.grade_label .. ' '.. playerName ..': {0}</div>',
        args = {toSay}
    })
else 
  TriggerClientEvent('chat:addMessage', source, {
    template = '<div style="padding: 0.45vw; margin: 0.05vw; background-color: rgba(205, 0, 0, 0.9); border-radius: 10px; word-wrap: break-word;"><i class="fas fa-exclamation"></i> You ain\'t part of the Familia fool! <i class="fas fa-exclamation"></i></div>',
    args = {}
  })
end
end, false)

-- INZERAT
-- TriggerEvent('es:addCommand', 'advert', function(source, args, rawCommand)
--   local characterName = GetCharacterName(source)
-- --  local msg = rawCommand:sub(8)
--   local xPlayer = ESX.GetPlayerFromId(source)
--   local characterName = GetCharacterName(source)
--   local phoneNumber = GetCharacterPhoneNumber(source)
--   local toSay = ''
--        for i=1,#args do
--     toSay = toSay .. args[i] .. ' ' -- Concats two strings together
--   end

--   if xPlayer.get('money') >= 250 then
--     TriggerClientEvent('chat:addMessage', -1, {
--         template = '<div style="padding: 0.3vw 0.8vw; margin: 0.5vw 0.5vw 0.5vw 0; border-radius:10px; background-color: rgba(67,142,94, 0.6); border-radius: 5px;"><strong style="font-size: 11pt;">[Advertisement] {0} (tel.č: {1}):</strong><br><p style="padding-top: .3vw">{2}</p></div>',
--           args = { characterName, phoneNumber, toSay }
--       })

--     xPlayer.removeMoney(250)

--     TriggerClientEvent('chat:addMessage', source, {
--       template = '^0[^1PLATBA^0] You paid $ 250 for an ad',
--       args = {}
--     })

--   else 
--     TriggerClientEvent('chat:addMessage', source, {
--       template = '^0[^1VAROVANIE^0] You do not have enough funds to pay for the ad ($ 250)',
--       args = {}
--     })
--   end
-- end, false)--]]

-- Get Character name
function GetCharacterName(source)
  local result = MySQL.Sync.fetchAll('SELECT firstname, lastname FROM users WHERE identifier = @identifier', {
    ['@identifier'] = GetPlayerIdentifiers(source)[1]
  })

  if result[1] and result[1].firstname and result[1].lastname then
    if Config.OnlyFirstname then
      return result[1].firstname
    else
      return ('%s %s'):format(result[1].firstname, result[1].lastname)
    end
  else
    return GetPlayerName(source)
  end
end

function GetCharacterJobName(source) 
  local result = MySQL.Sync.fetchAll('SELECT job FROM users WHERE identifier = @identifier', {
    ['@identifier'] = GetPlayerIdentifiers(source)[1]
  })

  if result[1] and result[1].job then
    return result[1].job
  end

  return nil
end

function GetCharacterPhoneNumber(source)
  local result = MySQL.Sync.fetchAll('SELECT phone_number FROM users WHERE identifier = @identifier', {
    ['@identifier'] = GetPlayerIdentifiers(source)[1]
  })

  if result[1] and result[1].phone_number then
    return result[1].phone_number
  end

  return nil
end
