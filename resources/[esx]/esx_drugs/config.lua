Config              = {}
Config.MarkerType   = 1
Config.DrawDistance = 100.0
Config.ZoneSize     = {x = 5.0, y = 5.0, z = 3.0}
Config.MarkerColor  = {r = 100, g = 204, b = 100}
Config.RequiredCopsCoke = 0
Config.RequiredCopsMeth = 0
Config.RequiredCopsWeed = 0
Config.RequiredCopsOpium = 0
Config.Locale = 'en'

Config.Zones = {
	CokeFarm = 		 {x=-536.627, y=-1794.165, z=21.405},
	CokeTreatment =  {x=-527.87, y=-1796.579, z=21.609},
	CokeResell = 	 {x=-1756.1984863281, y=427.31674194336, z=126.68292999268},
	MethFarm = 		 {x=-521.193, y=-1738.474, z=17.2},
	MethTreatment =  {x=-531.386, y=-1737.931, z=16.726},
	MethResell = 	 {x=-63.592178344727, y=-1224.0709228516, z=27.768648147583},
	WeedFarm = 		 {x=1758.635, y=-1603.022, z=112.67},
	WeedTreatment =  {x=1771.277, y=-1621.101, z=113.63},
	WeedResell = 	 {x=-54.249694824219, y=-1443.3666992188, z=31.068626403809},
	OpiumFarm = 	 {x=2433.725, y=4969.047, z=42.348},
	OpiumTreatment = {x=2434.384, y=4963.933, z=42.348},
	OpiumResell = 	 {x=2331.0881347656,y=2570.2250976562,z=46.681819915772}
}
