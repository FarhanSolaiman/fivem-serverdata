Config                            = {}

Config.DrawDistance               = 100.0
Config.MarkerType                 = 1
Config.MarkerSize                 = {x = 1.5, y = 1.5, z = 0.5}
Config.MarkerColor                = {r = 50, g = 50, b = 204}

Config.EnablePlayerManagement     = true
Config.EnableArmoryManagement     = true
Config.EnableESXIdentity          = true -- enable if you're using esx_identity
Config.EnableLicenses             = false -- enable if you're using esx_license

Config.EnableHandcuffTimer        = true -- enable handcuff timer? will unrestrain player after the time ends
Config.HandcuffTimer              = 10 * 60000 -- 10 mins

Config.EnableJobBlip              = true -- enable blips for cops on duty, requires esx_society
Config.EnableCustomPeds           = false -- enable custom peds in cloak room? See Config.CustomPeds below to customize peds

Config.EnableESXService           = false -- enable esx service?
Config.MaxInService               = 5

Config.Locale                     = 'en'

Config.GangStations = {

	Gangbase = {

		Blip = {
			Coords  = vector3(-330.26, 1106.37, 330.56),
			Sprite  = 84,
			Display = 4,
			Scale   = 0.8,
			Colour  = 1
		},

		Cloakrooms = {
			vector3(-321.84, 1087.34, 301.14)
		},

		Armories = {
			vector3(-327.11, 1080.61, 301.14)
		},

		Vehicles = {
			{
				Spawner = vector3(-337.91, 1129.24, 326.96),
				InsideShop = vector3(-339.95, 1135.07, 325.96),
				SpawnPoints = {
					{coords = vector3(-333.93, 1138.41, 325.96), heading = 66.59, radius = 6.0},
					{coords = vector3(-332.61, 1133.71, 325.96), heading = 47.68, radius = 6.0},
					{coords = vector3(-345.64, 1129.08, 325.96), heading = 341.82, radius = 6.0},
					{coords = vector3(-347.11, 1135.16, 325.96), heading = 326.33, radius = 6.0},
				}
			}
		},

		Helicopters = {
			{
				Spawner = vector3(-349.09, 1103.43, 330.56),
				InsideShop = vector3(-353.3, 1098.75, 329.56),
				SpawnPoints = {
					{coords = vector3(-353.3, 1098.75, 329.56), heading = 198.68, radius = 10.0}
				}
			}
		},

		BossActions = {
			vector3(-318.94, 1076.58, 322.5)
		}

	}

}

Config.AuthorizedWeapons = {

	falcons = {
		{weapon = 'WEAPON_PISTOL', components = {0, 0, 1000, 4000, 6000}, price = 10000},
		{weapon = 'WEAPON_ASSAULTSMG', components = {0, 3000, 1000, 4000, 6000, 8000}, price = 30000},
		{weapon = 'WEAPON_ASSAULTRIFLE', components = {0, 3000, nil, 1000, 6000,4000,nil,8000}, price = 50000},
		{weapon = 'WEAPON_PUMPSHOTGUN', components = {2000, 6000, 8000}, price = 70000},
	},

	hitmen = {
		{weapon = 'WEAPON_PISTOL', components = {0, 0, 1000, 4000, 6000}, price = 10000},
		{weapon = 'WEAPON_ASSAULTSMG', components = {0, 3000, 1000, 4000, 6000, 8000}, price = 30000},
		{weapon = 'WEAPON_ASSAULTRIFLE', components = {0, 3000, nil, 1000, 6000,4000,nil,8000}, price = 50000},
		{weapon = 'WEAPON_PUMPSHOTGUN', components = {2000, 6000, 8000}, price = 70000},
	},

	lieutenant = {
		{weapon = 'WEAPON_PISTOL', components = {0, 0, 1000, 4000, 6000}, price = 10000},
		{weapon = 'WEAPON_ASSAULTSMG', components = {0, 3000, 1000, 4000, 6000, 8000}, price = 30000},
		{weapon = 'WEAPON_ASSAULTRIFLE', components = {0, 3000, nil, 1000, 6000,4000,nil,8000}, price = 50000},
		{weapon = 'WEAPON_PUMPSHOTGUN', components = {2000, 6000, 8000}, price = 70000},
	},

	rhman = {
		{weapon = 'WEAPON_PISTOL', components = {0, 0, 1000, 4000, 6000}, price = 10000},
		{weapon = 'WEAPON_ASSAULTSMG', components = {0, 3000, 1000, 4000, 6000, 8000}, price = 30000},
		{weapon = 'WEAPON_ASSAULTRIFLE', components = {0, 3000, nil, 1000, 6000,4000,nil,8000}, price = 50000},
		{weapon = 'WEAPON_PUMPSHOTGUN', components = {2000, 6000, 8000}, price = 70000},
	},

	boss = {
		{weapon = 'WEAPON_PISTOL', components = {0, 0, 1000, 4000, 6000}, price = 10000},
		{weapon = 'WEAPON_ASSAULTSMG', components = {0, 3000, 1000, 4000, 6000, 8000}, price = 30000},
		{weapon = 'WEAPON_ASSAULTRIFLE', components = {0, 3000, nil, 1000, 6000,4000,nil,8000}, price = 50000},
		{weapon = 'WEAPON_PUMPSHOTGUN', components = {2000, 6000, 8000}, price = 70000},
	}
}

Config.AuthorizedVehicles = {
	car = {

		falcons = {
			{model = 'kuruma', price = 400000},
			{model = 'akuma', price = 505000}
		},

		hitmen = {
			{model = 'kuruma', price = 400000},
			{model = 'akuma', price = 505000}
		},

		lieutenant = {
			{model = 'kuruma', price = 400000},
			{model = 'schafter5', price = 800000},
			{model = 'akuma', price = 505000}
		},

		rhman = {
			{model = 'kuruma', price = 400000},
			{model = 'schafter5', price = 800000},
			{model = 'akuma', price = 505000},
			{model = 'granger', price = 705000}
		},

		boss = {
			{model = 'kuruma', price = 400000},
			{model = 'schafter5', price = 800000},
			{model = 'akuma', price = 505000},
			{model = 'granger', price = 705000}
		}
	},

	helicopter = {
		falcons = {},

		hitmen = {},

		lieutenant = {},

		rhman = {
			{model = 'maverick', price = 1000000}
		},

		boss = {
			{model = 'maverick', price = 1000000}
		},
	}
}

Config.CustomPeds = {
	shared = {
		{label = 'Sheriff Ped', maleModel = 's_m_y_sheriff_01', femaleModel = 's_f_y_sheriff_01'},
		{label = 'Police Ped', maleModel = 's_m_y_cop_01', femaleModel = 's_f_y_cop_01'}
	},

	officer = {},

	sergeant = {},

	lieutenant = {},

	captain = {
		{label = 'SWAT Ped', maleModel = 's_m_y_swat_01', femaleModel = 's_m_y_swat_01'}
	},

	corporal = {
		{label = 'SWAT Ped', maleModel = 's_m_y_swat_01', femaleModel = 's_m_y_swat_01'}
	},

	deputy = {
		{label = 'SWAT Ped', maleModel = 's_m_y_swat_01', femaleModel = 's_m_y_swat_01'}
	},

	boss = {
		{label = 'SWAT Ped', maleModel = 's_m_y_swat_01', femaleModel = 's_m_y_swat_01'}
	},

	undercover = {
		{label = 'SWAT Ped', maleModel = 's_m_y_swat_01', femaleModel = 's_m_y_swat_01'}
	},
}

-- CHECK SKINCHANGER CLIENT MAIN.LUA for matching elements
Config.Uniforms = {
	falcons = {
		male = {
			tshirt_1 = 21,  tshirt_2 = 0,
			torso_1 = 24,   torso_2 = 9,
			decals_1 = 0,   decals_2 = 0,
			arms = 22,
			pants_1 = 28,   pants_2 = 0,
			shoes_1 = 30,   shoes_2 = 1,
			helmet_1 = -1,  helmet_2 = 0,
			chain_1 = 1,    chain_2 = 0,
			ears_1 = -1,     ears_2 = 0
		},
		female = {
			tshirt_1 = 21,  tshirt_2 = 0,
			torso_1 = 24,   torso_2 = 9,
			decals_1 = 0,   decals_2 = 0,
			arms = 22,
			pants_1 = 28,   pants_2 = 0,
			shoes_1 = 30,   shoes_2 = 1,
			helmet_1 = -1,  helmet_2 = 0,
			chain_1 = 1,    chain_2 = 0,
			ears_1 = -1,     ears_2 = 0
		}
	},

	hitmen = {
		male = {
			tshirt_1 = 21,  tshirt_2 = 0,
			torso_1 = 24,   torso_2 = 9,
			decals_1 = 0,   decals_2 = 0,
			arms = 22,
			pants_1 = 28,   pants_2 = 0,
			shoes_1 = 30,   shoes_2 = 1,
			helmet_1 = -1,  helmet_2 = 0,
			chain_1 = 1,    chain_2 = 0,
			ears_1 = -1,     ears_2 = 0
		},
		female = {
			tshirt_1 = 21,  tshirt_2 = 0,
			torso_1 = 24,   torso_2 = 9,
			decals_1 = 0,   decals_2 = 0,
			arms = 22,
			pants_1 = 28,   pants_2 = 0,
			shoes_1 = 30,   shoes_2 = 1,
			helmet_1 = -1,  helmet_2 = 0,
			chain_1 = 1,    chain_2 = 0,
			ears_1 = -1,     ears_2 = 0
		}
	},

	lieutenant = {
		male = {
			tshirt_1 = 21,  tshirt_2 = 0,
			torso_1 = 24,   torso_2 = 9,
			decals_1 = 0,   decals_2 = 0,
			arms = 22,
			pants_1 = 28,   pants_2 = 0,
			shoes_1 = 30,   shoes_2 = 1,
			helmet_1 = -1,  helmet_2 = 0,
			chain_1 = 1,    chain_2 = 0,
			ears_1 = -1,     ears_2 = 0
		},
		female = {
			tshirt_1 = 21,  tshirt_2 = 0,
			torso_1 = 24,   torso_2 = 9,
			decals_1 = 0,   decals_2 = 0,
			arms = 22,
			pants_1 = 28,   pants_2 = 0,
			shoes_1 = 30,   shoes_2 = 1,
			helmet_1 = -1,  helmet_2 = 0,
			chain_1 = 1,    chain_2 = 0,
			ears_1 = -1,     ears_2 = 0
		}
	},

	rhman = {
		male = {
			tshirt_1 = 21,  tshirt_2 = 0,
			torso_1 = 24,   torso_2 = 9,
			decals_1 = 0,   decals_2 = 0,
			arms = 22,
			pants_1 = 28,   pants_2 = 0,
			shoes_1 = 30,   shoes_2 = 1,
			helmet_1 = -1,  helmet_2 = 0,
			chain_1 = 1,    chain_2 = 0,
			ears_1 = -1,     ears_2 = 0
		},
		female = {
			tshirt_1 = 21,  tshirt_2 = 0,
			torso_1 = 24,   torso_2 = 9,
			decals_1 = 0,   decals_2 = 0,
			arms = 22,
			pants_1 = 28,   pants_2 = 0,
			shoes_1 = 30,   shoes_2 = 1,
			helmet_1 = -1,  helmet_2 = 0,
			chain_1 = 1,    chain_2 = 0,
			ears_1 = -1,     ears_2 = 0
		}
	},

	boss = {
		male = {
			tshirt_1 = 21,  tshirt_2 = 0,
			torso_1 = 24,   torso_2 = 9,
			decals_1 = 0,   decals_2 = 0,
			arms = 22,
			pants_1 = 28,   pants_2 = 0,
			shoes_1 = 30,   shoes_2 = 1,
			helmet_1 = -1,  helmet_2 = 0,
			chain_1 = 1,    chain_2 = 0,
			ears_1 = -1,     ears_2 = 0
		},
		female = {
			tshirt_1 = 21,  tshirt_2 = 0,
			torso_1 = 24,   torso_2 = 9,
			decals_1 = 0,   decals_2 = 0,
			arms = 22,
			pants_1 = 28,   pants_2 = 0,
			shoes_1 = 30,   shoes_2 = 1,
			helmet_1 = -1,  helmet_2 = 0,
			chain_1 = 1,    chain_2 = 0,
			ears_1 = -1,     ears_2 = 0
		}
	}
}
