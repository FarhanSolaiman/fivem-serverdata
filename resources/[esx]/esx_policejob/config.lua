Config                            = {}

Config.DrawDistance               = 100.0
Config.MarkerType                 = 1
Config.MarkerSize                 = {x = 1.5, y = 1.5, z = 0.5}
Config.MarkerColor                = {r = 50, g = 50, b = 204}

Config.EnablePlayerManagement     = true
Config.EnableArmoryManagement     = true
Config.EnableESXIdentity          = true -- enable if you're using esx_identity
Config.EnableLicenses             = true -- enable if you're using esx_license

Config.EnableHandcuffTimer        = true -- enable handcuff timer? will unrestrain player after the time ends
Config.HandcuffTimer              = 10 * 60000 -- 10 mins

Config.EnableJobBlip              = true -- enable blips for cops on duty, requires esx_society
Config.EnableCustomPeds           = false -- enable custom peds in cloak room? See Config.CustomPeds below to customize peds

Config.EnableESXService           = false -- enable esx service?
Config.MaxInService               = 5

Config.Locale                     = 'en'

Config.PoliceStations = {

	LSPD = {

		Blip = {
			Coords  = vector3(425.1, -979.5, 30.7),
			Sprite  = 60,
			Display = 4,
			Scale   = 1.2,
			Colour  = 29
		},

		Cloakrooms = {
			vector3(452.6, -992.8, 30.6)
		},

		Armories = {
			vector3(451.7, -980.1, 30.6)
		},

		Vehicles = {
			{
				Spawner = vector3(454.6, -1017.4, 28.4),
				InsideShop = vector3(228.5, -993.5, -99.5),
				SpawnPoints = {
					{coords = vector3(438.4, -1018.3, 27.7), heading = 90.0, radius = 6.0},
					{coords = vector3(441.0, -1024.2, 28.3), heading = 90.0, radius = 6.0},
					{coords = vector3(453.5, -1022.2, 28.0), heading = 90.0, radius = 6.0},
					{coords = vector3(450.9, -1016.5, 28.1), heading = 90.0, radius = 6.0}
				}
			},

			{
				Spawner = vector3(473.3, -1018.8, 28.0),
				InsideShop = vector3(228.5, -993.5, -99.0),
				SpawnPoints = {
					{coords = vector3(475.9, -1021.6, 28.0), heading = 276.1, radius = 6.0},
					{coords = vector3(484.1, -1023.1, 27.5), heading = 302.5, radius = 6.0}
				}
			}
		},

		Helicopters = {
			{
				Spawner = vector3(461.1, -981.5, 43.6),
				InsideShop = vector3(477.0, -1106.4, 43.0),
				SpawnPoints = {
					{coords = vector3(449.5, -981.2, 43.6), heading = 92.6, radius = 10.0}
				}
			}
		},

		BossActions = {
			vector3(448.4, -973.2, 30.6)
		}

	}

}

Config.AuthorizedWeapons = {
	recruit = {
		{weapon = 'WEAPON_PISTOL', components = {0, 0, 1000, 4000, nil}, price = 10000},
		{weapon = 'WEAPON_NIGHTSTICK', price = 0},
		{weapon = 'WEAPON_STUNGUN', price = 1500},
		{weapon = 'WEAPON_FLASHLIGHT', price = 80}
	},

	officer = {
		{weapon = 'WEAPON_PISTOL', components = {0, 0, 1000, 4000, nil}, price = 10000},
		{weapon = 'WEAPON_COMBATPISTOL', components = {0, 0, 1000, 4000, nil}, price = 10000},
		{weapon = 'WEAPON_ASSAULTSMG', components = {0, 3000, 1000, 4000, 6000, nil}, price = 30000},
		{weapon = 'WEAPON_NIGHTSTICK', price = 0},
		{weapon = 'WEAPON_STUNGUN', price = 500},
		{weapon = 'WEAPON_FLASHLIGHT', price = 0}
	},

	sergeant = {
		{weapon = 'WEAPON_PISTOL', components = {0, 0, 1000, 4000, 6000}, price = 10000},
		{weapon = 'WEAPON_COMBATPISTOL', components = {0, 0, 1000, 4000, 6000}, price = 10000},
		{weapon = 'WEAPON_ASSAULTSMG', components = {0, 3000, 1000, 4000, 6000, 8000}, price = 30000},
		{weapon = 'WEAPON_COMBATPDW', components = {0, 3000, 6000, 1000, 1000, 4000}, price = 30000},
		{weapon = 'WEAPON_PUMPSHOTGUN', components = {2000, 6000, 8000}, price = 70000},
		{weapon = 'WEAPON_NIGHTSTICK', price = 0},
		{weapon = 'WEAPON_STUNGUN', price = 500},
		{weapon = 'WEAPON_FLASHLIGHT', price = 0}
	},

	lieutenant = {
		{weapon = 'WEAPON_PISTOL', components = {0, 0, 1000, 4000, 6000}, price = 10000},
		{weapon = 'WEAPON_COMBATPISTOL', components = {0, 0, 1000, 4000, 6000}, price = 10000},
		{weapon = 'WEAPON_ASSAULTSMG', components = {0, 3000, 1000, 4000, 6000, 8000}, price = 30000},
		{weapon = 'WEAPON_COMBATPDW', components = {0, 3000, 6000, 1000, 1000, 4000}, price = 30000},
		{weapon = 'WEAPON_ADVANCEDRIFLE', components = {0, 6000, 1000, 4000, 8000, 12000}, price = 50000},
		{weapon = 'WEAPON_PUMPSHOTGUN', components = {2000, 6000, 8000}, price = 70000},
		{weapon = 'WEAPON_NIGHTSTICK', price = 0},
		{weapon = 'WEAPON_STUNGUN', price = 500},
		{weapon = 'WEAPON_FLASHLIGHT', price = 0}
	},

	captain = {
		{weapon = 'WEAPON_PISTOL', components = {0, 0, 1000, 4000, 6000}, price = 10000},
		{weapon = 'WEAPON_COMBATPISTOL', components = {0, 0, 1000, 4000, 6000}, price = 10000},
		{weapon = 'WEAPON_ASSAULTSMG', components = {0, 3000, 1000, 4000, 6000, 8000}, price = 30000},
		{weapon = 'WEAPON_COMBATPDW', components = {0, 3000, 6000, 1000, 1000, 4000}, price = 30000},
		{weapon = 'WEAPON_ADVANCEDRIFLE', components = {0, 6000, 1000, 4000, 8000, 12000}, price = 50000},
		{weapon = 'WEAPON_PUMPSHOTGUN', components = {2000, 6000, 8000}, price = 70000},
		{weapon = 'WEAPON_NIGHTSTICK', price = 0},
		{weapon = 'WEAPON_STUNGUN', price = 500},
		{weapon = 'WEAPON_FLASHLIGHT', price = 0}
	},

	corporal = {
		{weapon = 'WEAPON_PISTOL', components = {0, 0, 1000, 4000, 6000}, price = 10000},
		{weapon = 'WEAPON_COMBATPISTOL', components = {0, 0, 1000, 4000, 6000}, price = 10000},
		{weapon = 'WEAPON_ASSAULTSMG', components = {0, 3000, 1000, 4000, 6000, 8000}, price = 30000},
		{weapon = 'WEAPON_COMBATPDW', components = {0, 3000, 6000, 1000, 1000, 4000}, price = 30000},
		{weapon = 'WEAPON_ADVANCEDRIFLE', components = {0, 6000, 1000, 4000, 8000, 12000}, price = 50000},
		{weapon = 'WEAPON_PUMPSHOTGUN', components = {2000, 6000, 8000}, price = 70000},
		{weapon = 'WEAPON_NIGHTSTICK', price = 0},
		{weapon = 'WEAPON_STUNGUN', price = 500},
		{weapon = 'WEAPON_FLASHLIGHT', price = 0}
	},

	deputy = {
		{weapon = 'WEAPON_PISTOL', components = {0, 0, 1000, 4000, 6000}, price = 10000},
		{weapon = 'WEAPON_COMBATPISTOL', components = {0, 0, 1000, 4000, 6000}, price = 10000},
		{weapon = 'WEAPON_ASSAULTSMG', components = {0, 3000, 1000, 4000, 6000, 8000}, price = 30000},
		{weapon = 'WEAPON_COMBATPDW', components = {0, 3000, 6000, 1000, 1000, 4000}, price = 30000},
		{weapon = 'WEAPON_ADVANCEDRIFLE', components = {0, 6000, 1000, 4000, 8000, 12000}, price = 50000},
		{weapon = 'WEAPON_PUMPSHOTGUN', components = {2000, 6000, 8000}, price = 70000},
		{weapon = 'WEAPON_NIGHTSTICK', price = 0},
		{weapon = 'WEAPON_STUNGUN', price = 500},
		{weapon = 'WEAPON_FLASHLIGHT', price = 0}
	},

	boss = {
		{weapon = 'WEAPON_PISTOL', components = {0, 0, 1000, 4000, 6000}, price = 10000},
		{weapon = 'WEAPON_COMBATPISTOL', components = {0, 0, 1000, 4000, 6000}, price = 10000},
		{weapon = 'WEAPON_ASSAULTSMG', components = {0, 3000, 1000, 4000, 6000, 8000}, price = 30000},
		{weapon = 'WEAPON_COMBATPDW', components = {0, 3000, 6000, 1000, 1000, 4000}, price = 30000},
		{weapon = 'WEAPON_ADVANCEDRIFLE', components = {0, 6000, 1000, 4000, 8000, 12000}, price = 50000},
		{weapon = 'WEAPON_PUMPSHOTGUN', components = {2000, 6000, 8000}, price = 70000},
		{weapon = 'WEAPON_NIGHTSTICK', price = 0},
		{weapon = 'WEAPON_STUNGUN', price = 500},
		{weapon = 'WEAPON_FLASHLIGHT', price = 0}
	},

	undercover = {
		{weapon = 'WEAPON_PISTOL', components = {0, 0, 1000, 4000, 6000}, price = 10000},
		{weapon = 'WEAPON_COMBATPISTOL', components = {0, 0, 1000, 4000, 6000}, price = 10000},
		{weapon = 'WEAPON_ASSAULTSMG', components = {0, 3000, 1000, 4000, 6000, 8000}, price = 30000},
		{weapon = 'WEAPON_COMBATPDW', components = {0, 3000, 6000, 1000, 1000, 4000}, price = 30000},
		{weapon = 'WEAPON_ADVANCEDRIFLE', components = {0, 6000, 1000, 4000, 8000, 12000}, price = 50000},
		{weapon = 'WEAPON_PUMPSHOTGUN', components = {2000, 6000, 8000}, price = 70000},
		{weapon = 'WEAPON_NIGHTSTICK', price = 0},
		{weapon = 'WEAPON_STUNGUN', price = 500},
		{weapon = 'WEAPON_FLASHLIGHT', price = 0}
	},
}

Config.AuthorizedVehicles = {
	car = {
		recruit = {
			{model = 'cvpileg', price = 100000},
			{model = 'tarleg', price = 305000}
		},

		officer = {
			{model = 'cvpileg', price = 100000},
			{model = 'tarleg', price = 305000},
			{model = 'chargleg2', price = 505000},
			{model = 'fpiuleg2', price = 405000},
		},

		sergeant = {
			{model = 'cvpileg', price = 100000},
			{model = 'tarleg', price = 305000},
			{model = 'chargleg2', price = 505000},
			{model = 'fpiuleg2', price = 405000},
			{model = 'chargleg4', price = 605000},
			{model = 'bikeleg', price = 305000}
		},

		lieutenant = {
			{model = 'cvpileg', price = 100000},
			{model = 'tarleg', price = 305000},
			{model = 'chargleg2', price = 505000},
			{model = 'fpiuleg2', price = 405000},
			{model = 'chargleg4', price = 605000},
			{model = 'ramleg', price = 705000},
			{model = 'taholeg3', price = 555000},
			{model = 'bikeleg2', price = 605000},
			{model = 'bikeleg', price = 305000},
			{model = 'riot', price = 700000}
		},

		captain = {
			{model = 'cvpileg', price = 100000},
			{model = 'tarleg', price = 305000},
			{model = 'chargleg2', price = 505000},
			{model = 'fpiuleg2', price = 405000},
			{model = 'chargleg4', price = 605000},
			{model = 'ramleg', price = 705000},
			{model = 'taholeg3', price = 555000},
			{model = 'sierraleg', price = 805000},
			{model = 'bikeleg2', price = 605000},
			{model = 'bikeleg', price = 305000},
			{model = 'riot', price = 700000},
			{model = 'pbus', price = 100000}
		},

		corporal = {
			{model = 'cvpileg', price = 100000},
			{model = 'tarleg', price = 305000},
			{model = 'chargleg2', price = 505000},
			{model = 'fpiuleg2', price = 405000},
			{model = 'chargleg4', price = 605000},
			{model = 'ramleg', price = 705000},
			{model = 'taholeg3', price = 555000},
			{model = 'sierraleg', price = 805000},
			{model = 'bikeleg2', price = 605000},
			{model = 'bikeleg', price = 305000},
			{model = 'polp1', price = 1305000},
			{model = 'riot', price = 700000},
			{model = 'pbus', price = 100000}
		},

		deputy = {
			{model = 'cvpileg', price = 100000},
			{model = 'tarleg', price = 305000},
			{model = 'chargleg2', price = 505000},
			{model = 'fpiuleg2', price = 405000},
			{model = 'chargleg4', price = 605000},
			{model = 'ramleg', price = 705000},
			{model = 'taholeg3', price = 555000},
			{model = 'sierraleg', price = 805000},
			{model = 'bikeleg2', price = 605000},
			{model = 'bikeleg', price = 305000},
			{model = 'polp1', price = 1305000},
			{model = 'riot', price = 700000},
			{model = 'pbus', price = 100000}
		},

		boss = {
			{model = 'cvpileg', price = 100000},
			{model = 'tarleg', price = 305000},
			{model = 'chargleg2', price = 505000},
			{model = 'fpiuleg2', price = 405000},
			{model = 'chargleg4', price = 605000},
			{model = 'ramleg', price = 705000},
			{model = 'taholeg3', price = 555000},
			{model = 'sierraleg', price = 805000},
			{model = 'bikeleg2', price = 605000},
			{model = 'bikeleg', price = 305000},
			{model = 'polp1', price = 1305000},
			{model = 'riot', price = 700000},
			{model = 'pbus', price = 100000}
		},

		undercover = {
			{model = 'cvpileg', price = 10000},
			{model = 'tarleg', price = 30500},
			{model = 'chargleg2', price = 50500},
			{model = 'fpiuleg2', price = 40500},
			{model = 'chargleg4', price = 60500},
			{model = 'ramleg', price = 70500},
			{model = 'taholeg3', price = 55500},
			{model = 'sierraleg', price = 80500},
			{model = 'bikeleg2', price = 60500},
			{model = 'bikeleg', price = 30500},
			{model = 'riot', price = 70000}
		},
	},

	helicopter = {
		recruit = {},

		officer = {},

		sergeant = {},

		lieutenant = {
			{model = 'polmav', props = {modLivery = 0}, price = 200000}
		},

		captain = {
			{model = 'polmav', props = {modLivery = 0}, price = 100000}
		},

		corporal = {
			{model = 'polmav', props = {modLivery = 0}, price = 100000}
		},

		deputy = {
			{model = 'polmav', props = {modLivery = 0}, price = 100000}
		},

		boss = {
			{model = 'polmav', props = {modLivery = 0}, price = 100000}
		},

		undercover = {
			{model = 'polmav', props = {modLivery = 0}, price = 100000}
		},
	}
}

Config.CustomPeds = {
	shared = {
		{label = 'Sheriff Ped', maleModel = 's_m_y_sheriff_01', femaleModel = 's_f_y_sheriff_01'},
		{label = 'Police Ped', maleModel = 's_m_y_cop_01', femaleModel = 's_f_y_cop_01'}
	},

	recruit = {},

	officer = {},

	sergeant = {},

	lieutenant = {},

	captain = {
		{label = 'SWAT Ped', maleModel = 's_m_y_swat_01', femaleModel = 's_m_y_swat_01'}
	},

	corporal = {
		{label = 'SWAT Ped', maleModel = 's_m_y_swat_01', femaleModel = 's_m_y_swat_01'}
	},

	deputy = {
		{label = 'SWAT Ped', maleModel = 's_m_y_swat_01', femaleModel = 's_m_y_swat_01'}
	},

	boss = {
		{label = 'SWAT Ped', maleModel = 's_m_y_swat_01', femaleModel = 's_m_y_swat_01'}
	},

	undercover = {
		{label = 'SWAT Ped', maleModel = 's_m_y_swat_01', femaleModel = 's_m_y_swat_01'}
	},
}

-- CHECK SKINCHANGER CLIENT MAIN.LUA for matching elements
Config.Uniforms = {
	recruit = {
		male = {
			tshirt_1 = 15,  tshirt_2 = 0,
			torso_1 = 93,   torso_2 = 0,
			decals_1 = 0,   decals_2 = 0,
			arms = 19,
			pants_1 = 24,   pants_2 = 0,
			shoes_1 = 10,   shoes_2 = 0,
			helmet_1 = -1,  helmet_2 = 0,
			chain_1 = 1,    chain_2 = 0,
			ears_1 = -1,     ears_2 = 0
		},
		female = {
			tshirt_1 = 35,  tshirt_2 = 0,
			torso_1 = 84,   torso_2 = 0,
			decals_1 = 0,   decals_2 = 0,
			arms = 44,
			pants_1 = 34,   pants_2 = 0,
			shoes_1 = 77,   shoes_2 = 0,
			helmet_1 = -1,  helmet_2 = 0,
			chain_1 = 1,    chain_2 = 0,
			ears_1 = -1,     ears_2 = 0
		}
	},

	officer = {
		male = {
			tshirt_1 = 15,  tshirt_2 = 0,
			torso_1 = 93,   torso_2 = 0,
			decals_1 = 0,   decals_2 = 0,
			arms = 19,
			pants_1 = 24,   pants_2 = 0,
			shoes_1 = 10,   shoes_2 = 0,
			helmet_1 = -1,  helmet_2 = 0,
			chain_1 = 1,    chain_2 = 0,
			ears_1 = -1,     ears_2 = 0
		},
		female = {
			tshirt_1 = 35,  tshirt_2 = 0,
			torso_1 = 84,   torso_2 = 0,
			decals_1 = 0,   decals_2 = 0,
			arms = 44,
			pants_1 = 34,   pants_2 = 0,
			shoes_1 = 77,   shoes_2 = 0,
			helmet_1 = -1,  helmet_2 = 0,
			chain_1 = 1,    chain_2 = 0,
			ears_1 = -1,     ears_2 = 0
		}
	},

	sergeant = {
		male = {
			tshirt_1 = 15,  tshirt_2 = 0,
			torso_1 = 93,   torso_2 = 0,
			decals_1 = 0,   decals_2 = 0,
			arms = 19,
			pants_1 = 24,   pants_2 = 0,
			shoes_1 = 10,   shoes_2 = 0,
			helmet_1 = -1,  helmet_2 = 0,
			chain_1 = 1,    chain_2 = 0,
			ears_1 = -1,     ears_2 = 0
		},
		female = {
			tshirt_1 = 35,  tshirt_2 = 0,
			torso_1 = 84,   torso_2 = 0,
			decals_1 = 0,   decals_2 = 0,
			arms = 44,
			pants_1 = 34,   pants_2 = 0,
			shoes_1 = 77,   shoes_2 = 0,
			helmet_1 = -1,  helmet_2 = 0,
			chain_1 = 1,    chain_2 = 0,
			ears_1 = -1,     ears_2 = 0
		}
	},

	lieutenant = {
		male = {
			tshirt_1 = 15,  tshirt_2 = 0,
			torso_1 = 93,   torso_2 = 0,
			decals_1 = 0,   decals_2 = 0,
			arms = 19,
			pants_1 = 24,   pants_2 = 0,
			shoes_1 = 10,   shoes_2 = 0,
			helmet_1 = -1,  helmet_2 = 0,
			chain_1 = 1,    chain_2 = 0,
			ears_1 = -1,     ears_2 = 0
		},
		female = {
			tshirt_1 = 35,  tshirt_2 = 0,
			torso_1 = 84,   torso_2 = 0,
			decals_1 = 0,   decals_2 = 0,
			arms = 44,
			pants_1 = 34,   pants_2 = 0,
			shoes_1 = 77,   shoes_2 = 0,
			helmet_1 = -1,  helmet_2 = 0,
			chain_1 = 1,    chain_2 = 0,
			ears_1 = -1,     ears_2 = 0
		}
	},

	captain = {
		male = {
			tshirt_1 = 15,  tshirt_2 = 0,
			torso_1 = 93,   torso_2 = 0,
			decals_1 = 0,   decals_2 = 0,
			arms = 19,
			pants_1 = 24,   pants_2 = 0,
			shoes_1 = 10,   shoes_2 = 0,
			helmet_1 = -1,  helmet_2 = 0,
			chain_1 = 1,    chain_2 = 0,
			ears_1 = -1,     ears_2 = 0
		},
		female = {
			tshirt_1 = 35,  tshirt_2 = 0,
			torso_1 = 84,   torso_2 = 0,
			decals_1 = 0,   decals_2 = 0,
			arms = 44,
			pants_1 = 34,   pants_2 = 0,
			shoes_1 = 77,   shoes_2 = 0,
			helmet_1 = -1,  helmet_2 = 0,
			chain_1 = 1,    chain_2 = 0,
			ears_1 = -1,     ears_2 = 0
		}
	},

	corporal = {
		male = {
			tshirt_1 = 15,  tshirt_2 = 0,
			torso_1 = 93,   torso_2 = 0,
			decals_1 = 0,   decals_2 = 0,
			arms = 19,
			pants_1 = 24,   pants_2 = 0,
			shoes_1 = 10,   shoes_2 = 0,
			helmet_1 = -1,  helmet_2 = 0,
			chain_1 = 1,    chain_2 = 0,
			ears_1 = -1,     ears_2 = 0
		},
		female = {
			tshirt_1 = 35,  tshirt_2 = 0,
			torso_1 = 84,   torso_2 = 0,
			decals_1 = 0,   decals_2 = 0,
			arms = 44,
			pants_1 = 34,   pants_2 = 0,
			shoes_1 = 77,   shoes_2 = 0,
			helmet_1 = -1,  helmet_2 = 0,
			chain_1 = 1,    chain_2 = 0,
			ears_1 = -1,     ears_2 = 0
		}
	},

	deputy = {
		male = {
			tshirt_1 = 15,  tshirt_2 = 0,
			torso_1 = 93,   torso_2 = 0,
			decals_1 = 0,   decals_2 = 0,
			arms = 19,
			pants_1 = 24,   pants_2 = 0,
			shoes_1 = 10,   shoes_2 = 0,
			helmet_1 = -1,  helmet_2 = 0,
			chain_1 = 1,    chain_2 = 0,
			ears_1 = -1,     ears_2 = 0
		},
		female = {
			tshirt_1 = 35,  tshirt_2 = 0,
			torso_1 = 84,   torso_2 = 0,
			decals_1 = 0,   decals_2 = 0,
			arms = 44,
			pants_1 = 34,   pants_2 = 0,
			shoes_1 = 77,   shoes_2 = 0,
			helmet_1 = -1,  helmet_2 = 0,
			chain_1 = 1,    chain_2 = 0,
			ears_1 = -1,     ears_2 = 0
		}
	},

	boss = {
		male = {
			tshirt_1 = 15,  tshirt_2 = 0,
			torso_1 = 93,   torso_2 = 0,
			decals_1 = 0,   decals_2 = 0,
			arms = 19,
			pants_1 = 24,   pants_2 = 0,
			shoes_1 = 10,   shoes_2 = 0,
			helmet_1 = -1,  helmet_2 = 0,
			chain_1 = 1,    chain_2 = 0,
			ears_1 = -1,     ears_2 = 0
		},
		female = {
			tshirt_1 = 35,  tshirt_2 = 0,
			torso_1 = 84,   torso_2 = 0,
			decals_1 = 0,   decals_2 = 0,
			arms = 44,
			pants_1 = 34,   pants_2 = 0,
			shoes_1 = 77,   shoes_2 = 0,
			helmet_1 = -1,  helmet_2 = 0,
			chain_1 = 1,    chain_2 = 0,
			ears_1 = -1,     ears_2 = 0
		}
	},

	undercover = {
		male = {
			tshirt_1 = 15,  tshirt_2 = 0,
			torso_1 = 93,   torso_2 = 0,
			decals_1 = 0,   decals_2 = 0,
			arms = 19,
			pants_1 = 24,   pants_2 = 0,
			shoes_1 = 10,   shoes_2 = 0,
			helmet_1 = -1,  helmet_2 = 0,
			chain_1 = 1,    chain_2 = 0,
			ears_1 = -1,     ears_2 = 0
		},
		female = {
			tshirt_1 = 35,  tshirt_2 = 0,
			torso_1 = 84,   torso_2 = 0,
			decals_1 = 0,   decals_2 = 0,
			arms = 44,
			pants_1 = 34,   pants_2 = 0,
			shoes_1 = 77,   shoes_2 = 0,
			helmet_1 = -1,  helmet_2 = 0,
			chain_1 = 1,    chain_2 = 0,
			ears_1 = -1,     ears_2 = 0
		}
	},

	bullet_wear = {
		male = {
			bproof_1 = 7,  bproof_2 = 0
		},
		female = {
			bproof_1 = 7,  bproof_2 = 0
		}
	},

	gilet_wear = {
		male = {
			tshirt_1 = 59,  tshirt_2 = 1
		},
		female = {
			tshirt_1 = 36,  tshirt_2 = 1
		}
	}
}
