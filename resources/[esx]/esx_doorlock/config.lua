Config = {}
Config.Locale = 'en'

Config.DoorList = {

	--
	-- Mission Row First Floor
	--

	-- Entrance Doors
	{
		textCoords = vector3(434.7, -982.0, 31.5),
		authorizedJobs = {'police','offpolice'},
		locked = true,
		maxDistance = 2.5,
		doors = {
			{objHash = GetHashKey('v_ilev_ph_door01'), objHeading = 270.0, objCoords = vector3(434.7, -980.6, 30.8)},
			{objHash = GetHashKey('v_ilev_ph_door002'), objHeading = 270.0, objCoords = vector3(434.7, -983.2, 30.8)}
		}
	},

	-- To locker room & roof
	{
		objHash = GetHashKey('v_ilev_ph_gendoor004'),
		objHeading = 90.0,
		objCoords = vector3(449.6, -986.4, 30.6),
		textCoords = vector3(450.1, -986.3, 31.7),
		authorizedJobs = {'police','offpolice'},
		locked = true,
		maxDistance = 1.25
	},

	-- Rooftop
	{
		objHash = GetHashKey('v_ilev_gtdoor02'),
		objHeading = 90.0,
		objCoords = vector3(464.3, -984.6, 43.8),
		textCoords = vector3(464.3, -984.0, 44.8),
		authorizedJobs = {'police','offpolice'},
		locked = true,
		maxDistance = 1.25
	},

	-- Hallway to roof
	{
		objHash = GetHashKey('v_ilev_arm_secdoor'),
		objHeading = 90.0,
		objCoords = vector3(461.2, -985.3, 30.8),
		textCoords = vector3(461.5, -986.0, 31.5),
		authorizedJobs = {'police','offpolice'},
		locked = true,
		maxDistance = 1.25
	},

	-- Armory
	{
		objHash = GetHashKey('v_ilev_arm_secdoor'),
		objHeading = 270.0,
		objCoords = vector3(452.6, -982.7, 30.6),
		textCoords = vector3(453.0, -982.6, 31.7),
		authorizedJobs = {'police','offpolice'},
		locked = true,
		maxDistance = 1.25
	},

	-- Captain Office
	{
		objHash = GetHashKey('v_ilev_ph_gendoor002'),
		objHeading = 180.0,
		objCoords = vector3(447.2, -980.6, 30.6),
		textCoords = vector3(447.2, -980.0, 31.7),
		authorizedJobs = {'police','offpolice'},
		locked = true,
		maxDistance = 1.25
	},

	-- To downstairs (double doors)
	{
		textCoords = vector3(444.6, -989.4, 31.7),
		authorizedJobs = {'police','offpolice'},
		locked = true,
		maxDistance = 4,
		doors = {
			{objHash = GetHashKey('v_ilev_ph_gendoor005'), objHeading = 180.0, objCoords = vector3(443.9, -989.0, 30.6)},
			{objHash = GetHashKey('v_ilev_ph_gendoor005'), objHeading = 0.0, objCoords = vector3(445.3, -988.7, 30.6)}
		}
	},

	--
	-- Mission Row Cells
	--

	-- Main Cells
	{
		objHash = GetHashKey('v_ilev_ph_cellgate'),
		objHeading = 0.0,
		objCoords = vector3(463.8, -992.6, 24.9),
		textCoords = vector3(463.3, -992.6, 25.1),
		authorizedJobs = {'police','offpolice'},
		locked = true,
		maxDistance = 1.25
	},

	-- Cell 1
	{
		objHash = GetHashKey('v_ilev_ph_cellgate'),
		objHeading = 270.0,
		objCoords = vector3(462.3, -993.6, 24.9),
		textCoords = vector3(461.8, -993.3, 25.0),
		authorizedJobs = {'police','offpolice'},
		locked = true,
		maxDistance = 1.25
	},

	-- Cell 2
	{
		objHash = GetHashKey('v_ilev_ph_cellgate'),
		objHeading = 90.0,
		objCoords = vector3(462.3, -998.1, 24.9),
		textCoords = vector3(461.8, -998.8, 25.0),
		authorizedJobs = {'police','offpolice'},
		locked = true,
		maxDistance = 1.25
	},

	-- Cell 3
	{
		objHash = GetHashKey('v_ilev_ph_cellgate'),
		objHeading = 90.0,
		objCoords = vector3(462.7, -1001.9, 24.9),
		textCoords = vector3(461.8, -1002.4, 25.0),
		authorizedJobs = {'police','offpolice'},
		locked = true,
		maxDistance = 1.25
	},

	-- To Back
	{
		objHash = GetHashKey('v_ilev_gtdoor'),
		objHeading = 0.0,
		objCoords = vector3(463.4, -1003.5, 25.0),
		textCoords = vector3(464.0, -1003.5, 25.5),
		authorizedJobs = {'police','offpolice'},
		locked = true,
		maxDistance = 1.25
	},

	--
	-- Mission Row Back
	--

	-- Back (double doors)
	{
		textCoords = vector3(468.6, -1014.4, 27.1),
		authorizedJobs = {'police','offpolice'},
		locked = true,
		maxDistance = 4,
		doors = {
			{objHash = GetHashKey('v_ilev_rc_door2'), objHeading = 0.0, objCoords  = vector3(467.3, -1014.4, 26.5)},
			{objHash = GetHashKey('v_ilev_rc_door2'), objHeading = 180.0, objCoords  = vector3(469.9, -1014.4, 26.5)}
		}
	},

	{
		textCoords = vector3(445.74, -996.99, 30.68),
		authorizedJobs = {'police','offpolice'},
		locked = true,
		maxDistance = 4,
		doors = {
			{objHash = GetHashKey('v_ilev_rc_door2'), objHeading = 180.0, objCoords  = vector3(446.54, -997.03, 30.68)},
			{objHash = GetHashKey('v_ilev_rc_door2'), objHeading = 0.0, objCoords  = vector3(445.22, -997.03, 30.68)}
		}
	},

	-- Back Gate
	{
		objHash = GetHashKey('hei_prop_station_gate'),
		objHeading = 90.0,
		objCoords = vector3(488.8, -1017.2, 27.1),
		textCoords = vector3(488.8, -1020.2, 30.0),
		authorizedJobs = {'police','offpolice'},
		locked = true,
		maxDistance = 14,
		size = 2
	},

	{
		objName = GetHashKey('prop_gate_docks_ld'),
		objHeading = 179.19,
		objCoords  = vector3(410.91, -1026.61, 29.39),
		textCoords = vector3(410.79, -1023.99, 29.39),
		authorizedJobs = {'police','offpolice'},
		locked = true,
		distance = 14,
		size = 2
	},
	
	{
		objName = GetHashKey('prop_fnclink_09gate1'),
		objCoords  = vector3(423.716, -992.16, 27.71),
		textCoords = vector3(423.716, -992.16, 30.71),
		authorizedJobs = {'police','offpolice'},
		locked = true,
		distance = 2.5
	},

	--
	-- Sandy Shores
	--

	-- Entrance
	-- {
	-- 	objHash = GetHashKey('v_ilev_shrfdoor'),
	-- 	objHeading = 30.0,
	-- 	objCoords = vector3(1855.1, 3683.5, 34.2),
	-- 	textCoords = vector3(1855.1, 3683.5, 35.0),
	-- 	authorizedJobs = {'police','offpolice'},
	-- 	locked = false,
	-- 	maxDistance = 1.25
	-- },

	--
	-- Paleto Bay
	--

	-- Entrance (double doors)
	-- {
	-- 	textCoords = vector3(-443.5, 6016.3, 32.0),
	-- 	authorizedJobs = {'police','offpolice'},
	-- 	locked = false,
	-- 	maxDistance = 2.5,
	-- 	doors = {
	-- 		{objHash = GetHashKey('v_ilev_shrf2door'), objHeading = 315.0, objCoords  = vector3(-443.1, 6015.6, 31.7)},
	-- 		{objHash = GetHashKey('v_ilev_shrf2door'), objHeading = 135.0, objCoords  = vector3(-443.9, 6016.6, 31.7)}
	-- 	}
	-- },

	--
	-- Bolingbroke Penitentiary
	--

	-- Entrance (Two big gates)
	{
		objHash = GetHashKey('prop_gate_prison_01'),
		objCoords = vector3(1844.9, 2604.8, 44.6),
		textCoords = vector3(1844.9, 2608.5, 48.0),
		authorizedJobs = {'police','offpolice'},
		locked = true,
		maxDistance = 12,
		size = 2
	},

	{
		objHash = GetHashKey('prop_gate_prison_01'),
		objCoords = vector3(1818.5, 2604.8, 44.6),
		textCoords = vector3(1818.5, 2608.4, 48.0),
		authorizedJobs = {'police','offpolice'},
		locked = true,
		maxDistance = 12,
		size = 2
	},

	-- Mechshop
	-- {
	-- 	textCoords = vector3(-1176.92, -1729.96, 4.45),
	-- 	authorizedJobs = {'mechanic','offmechanic'},
	-- 	locked = true,
	-- 	maxDistance = 4,
	-- 	doors = {
	-- 		{objHash = GetHashKey('prop_faceoffice_door_l'), objHeading = 35.3, objCoords  = vector3(-1175.68, -1729.19, 4.97)},
	-- 		{objHash = GetHashKey('prop_faceoffice_door_l'), objHeading = 215.3, objCoords  = vector3(-1178.11, -1730.91, 4.97)}
	-- 	}
	-- },
	-- {
	-- 	textCoords = vector3(-1157.82, -1716.58, 4.45),
	-- 	authorizedJobs = {'mechanic','offmechanic','offmechanic'},
	-- 	locked = true,
	-- 	maxDistance = 4,
	-- 	doors = {
	-- 		{objHash = GetHashKey('prop_faceoffice_door_l'), objHeading = 35.3, objCoords  = vector3(-1156.62, -1715.75, 4.97)},
	-- 		{objHash = GetHashKey('prop_faceoffice_door_l'), objHeading = 215.3, objCoords  = vector3(-1159.09, -1717.48, 4.97)}
	-- 	}
	-- },
	-- {
	-- 	objHash = GetHashKey('apa_prop_ss1_mpint_garage2'),
	-- 	objCoords = vector3(-1196.936, -1733.449, 5.656374),
	-- 	textCoords = vector3(-1196.936, -1733.449, 4.44),
	-- 	authorizedJobs = {'mechanic','offmechanic'},
	-- 	locked = true,
	-- 	maxDistance = 4,
	-- 	size = 2
	-- },
	-- {
	-- 	objHash = GetHashKey('apa_prop_ss1_mpint_garage2'),
	-- 	objCoords = vector3(-1190.505, -1728.999, 5.677717),
	-- 	textCoords = vector3(-1190.505, -1728.999, 4.44),
	-- 	authorizedJobs = {'mechanic','offmechanic'},
	-- 	locked = true,
	-- 	maxDistance = 4,
	-- 	size = 2
	-- },
	-- {
	-- 	objHash = GetHashKey('apa_prop_ss1_mpint_garage2'),
	-- 	objCoords = vector3(-1203.367, -1737.88, 5.680458),
	-- 	textCoords = vector3(-1203.367, -1737.88, 4.44),
	-- 	authorizedJobs = {'mechanic','offmechanic'},
	-- 	locked = true,
	-- 	maxDistance = 4,
	-- 	size = 2
	-- },
	-- {
	-- 	objHash = GetHashKey('apa_prop_ss1_mpint_garage2'),
	-- 	objCoords = vector3(-1208.886, -1741.814, 5.732911),
	-- 	textCoords = vector3(-1208.886, -1741.814, 4.44),
	-- 	authorizedJobs = {'mechanic','offmechanic'},
	-- 	locked = true,
	-- 	maxDistance = 4,
	-- 	size = 2
	-- },
	{
		objHash = GetHashKey('lr_prop_supermod_door_01'),
		objCoords = vector3(-205.5688, -1310.6566, 29.29572),
		textCoords = vector3(-205.5688, -1310.6566, 31.29572),
		authorizedJobs = {'mechanic','offmechanic'},
		locked = true,
		maxDistance = 5,
		size = 2
	},

	-- Surenos Mansion
	{
		objHash = GetHashKey('prop_lrggate_06a'),
		objCoords = vector3(-351.0918 ,1158.469 ,324.4586),
		textCoords = vector3(-348.57 ,1158.93 ,326.4586),
		authorizedJobs = {'surenos'},
		locked = true,
		maxDistance = 5,
		size = 2
	},

	-- -- Central Bank
	-- {
	-- 	objName = 'hei_v_ilev_bk_gate2_pris',
	-- 	objCoords  = vector3(261.99899291992, 221.50576782227, 106.68346405029),
	-- 	textCoords = vector3(261.99899291992, 221.50576782227, 107.68346405029),
	-- 	authorizedJobs = { 'police' },
	-- 	locked = true,
	-- 	distance = 12,
	-- 	size = 2
	-- },
}