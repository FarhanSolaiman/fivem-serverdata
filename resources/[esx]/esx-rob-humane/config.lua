Config = {}
Config.Locale = 'en'
Config.NumberOfCopsRequired = 7

Banks = {
	["humane_labs"] = {
		position = { ['x'] = 3536.17, ['y'] = 3660.11, ['z'] = 28.12 },
		reward = math.random(5000000,7500000),
		nameofbank = "Humane Labs",
		lastrobbed = 0
	},
}
