Config = {}

Config.Locale = 'en'

Config.EnableCash       = false
Config.EnableBlackMoney = true
Config.EnableInventory  = true
Config.EnableItems      = true
Config.EnableWeapons    = true