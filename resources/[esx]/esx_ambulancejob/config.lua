Config                            = {}

Config.DrawDistance               = 100.0

Config.Marker                     = { type = -1, x = 1.5, y = 1.5, z = 0.5, r = 102, g = 0, b = 102, a = 100, rotate = false }

Config.ReviveReward               = 8000  -- revive reward, set to 0 if you don't want it enabled
Config.AntiCombatLog              = true -- enable anti-combat logging?
Config.LoadIpl                    = false
Config.Locale = 'en'

local second = 1000
local minute = 60 * second

Config.EarlyRespawnTimer          = 10 * minute  -- Time til respawn is available
Config.BleedoutTimer              = 30 * minute -- Time til the player bleeds out

Config.EnablePlayerManagement     = true

Config.RemoveWeaponsAfterRPDeath  = true
Config.RemoveCashAfterRPDeath     = true
Config.RemoveItemsAfterRPDeath    = true

-- Let the player pay for respawning early, only if he can afford it.
Config.EarlyRespawnFine           = true
Config.EarlyRespawnFineAmount     = 20000

Config.RespawnPoint = { coords = vector3(341.0, -1397.3, 32.5), heading = 48.5}

Config.Hospitals = {

	PillBoxHospital = {

		Blip = {
			coords = vector3(321.83, -606.12, 29.29),
			sprite = 61,
			scale  = 1.5,
			color  = 2
		},

		AmbulanceActions = {
			vector3(336.18, -580.35, 27.89),
			vector3(301.55, -599.49, 42.28)
		},

		Pharmacies = {
			vector3(338.29, -585.7, 27.9),
			vector3(311.92, -597.37, 42.28)
		},

		Vehicles = {
			{
				Spawner = vector3(361.01, -580.89, 28.82),
				InsideShop = vector3(327.9, -548.08, 27.92),
				Marker = { type = 36, x = 1.0, y = 1.0, z = 1.0, r = 100, g = 50, b = 200, a = 100, rotate = true },
				SpawnPoints = {
					{ coords = vector3(364.33, -591.49, 27.79), heading = 159.5, radius = 10.0 }
				}
			},
			{
				Spawner = vector3(300.56, -579.04, 43.26),
				InsideShop = vector3(327.9, -548.08, 27.92),
				Marker = { type = 36, x = 1.0, y = 1.0, z = 1.0, r = 100, g = 50, b = 200, a = 100, rotate = true },
				SpawnPoints = {
					{ coords = vector3(290.93, -589.23, 42.19), heading = 335.62, radius = 10.0 }
				}
			}
		},

		Helicopters = {
			{
				Spawner = vector3(338.4, -588.29, 74.15),
				InsideShop = vector3(351.95, -588.39, 73.26),
				Marker = { type = 34, x = 1.5, y = 1.5, z = 1.5, r = 100, g = 150, b = 150, a = 100, rotate = true },
				SpawnPoints = {
					{ coords = vector3(351.95, -588.39, 73.26), heading = 245.56, radius = 14.0 }
				}
			}
		},

		FastTravels = {
			-- {
			-- 	From = vector3(327.33, -603.3, 43.28),
			-- 	To = { coords = vector3(350.69, -588.03, 74.17), heading = 245.5 },
			-- 	Marker = { type = 1, x = 2.0, y = 2.0, z = 0.5, r = 102, g = 0, b = 102, rotate = false }
			-- },
			-- {
			-- 	From = vector3(338.53, -583.8, 74.17),
			-- 	To = { coords = vector3(329.68, -600.79, 43.28), heading = 80.5 },
			-- 	Marker = { type = 1, x = 2.0, y = 2.0, z = 0.5, r = 102, g = 0, b = 102, rotate = false }
			-- },
			
		},

		FastTravelsPrompt = {
			-- {
			-- 	From = vector3(332.227, -595.82, 43.28),
			-- 	To = { coords = vector3(280.07, -1348.97, 24.54), heading = 319.7 },
			-- 	Marker = { type = 1, x = 1.5, y = 1.5, z = 0.5, r = 102, g = 0, b = 102, rotate = false },
			-- 	Prompt = _U('fast_travel')
			-- },

			-- {
			-- 	From = vector3(280.07, -1348.97, 24.54),
			-- 	To = { coords = vector3(332.227, -595.82, 43.28), heading = 160.55 },
			-- 	Marker = { type = 1, x = 1.5, y = 1.5, z = 0.5, r = 102, g = 0, b = 102, rotate = false },
			-- 	Prompt = _U('fast_travel')
			-- }
		}

	}
}

Config.AuthorizedVehicles = {

	intern = {
		{ model = 'ambulance', label = 'Ambulance Van', price = 100000}
	},

	physician = {
		{ model = 'ambulance', label = 'Ambulance Van', price = 100000}
	},

	nurse = {
		{ model = 'ambulance', label = 'Ambulance Van', price = 100000}
	},
	
	doctor = {
		{ model = 'ambulance', label = 'Ambulance Van', price = 100000},
		{ model = 'qrv', label = 'Ford Explorer EMS', price = 300000}
	},

	specialist = {
		{ model = 'ambulance', label = 'Ambulance Van', price = 100000},
		{ model = 'qrv', label = 'Ford Explorer EMS', price = 300000}
	},

	surgeon = {
		{ model = 'ambulance', label = 'Ambulance Van', price = 100000},
		{ model = 'dodgeems', label = 'Dodge Charger EMS', price = 300000},
		{ model = 'qrv', label = 'Ford Explorer EMS', price = 500000}
	},

	administrator = {
		{ model = 'ambulance', label = 'Ambulance Van', price = 100000},
		{ model = 'dodgeems', label = 'Dodge Charger EMS', price = 300000},
		{ model = 'qrv', label = 'Ford Explorer EMS', price = 500000}
	},

	boss = {
		{ model = 'ambulance', label = 'Ambulance Van', price = 100000},
		{ model = 'dodgeems', label = 'Dodge Charger EMS', price = 300000},
		{ model = 'qrv', label = 'Ford Explorer EMS', price = 500000}
	}

}

Config.AuthorizedHelicopters = {

	intern = {},

	physician = {},

	nurse = {},

	doctor = {
		{ model = 'polmav', props = {modLivery = 1}, label = 'Ambulance Maverick', price = 700000 }
	},

	specialist = {
		{ model = 'polmav', props = {modLivery = 1}, label = 'Ambulance Maverick', price = 700000 }
	},

	surgeon = {
		{ model = 'polmav', props = {modLivery = 1}, label = 'Ambulance Maverick', price = 700000 },
		{ model = 'seasparrow', label = 'Sea Sparrow', price = 1000000 }
	},

	administrator = {
		{ model = 'polmav', props = {modLivery = 1}, label = 'Ambulance Maverick', price = 700000 },
		{ model = 'seasparrow', label = 'Sea Sparrow', price = 1000000 }
	},

	boss = {
		{ model = 'polmav', props = {modLivery = 1}, label = 'Ambulance Maverick', price = 700000 },
		{ model = 'seasparrow', label = 'Sea Sparrow', price = 1000000 }
	}

}
