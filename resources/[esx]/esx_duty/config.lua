Config                            = {}
Config.DrawDistance               = 100.0
--language currently available EN and SV
Config.Locale                     = 'en'

Config.Zones = {

  PoliceDuty = {
    Pos   = { x = 439.825, y = -975.693, z = 29.691 },
    Size  = { x = 2.5, y = 2.5, z = 1.5 },
    Color = { r = 0, g = 255, b = 0 },  
    Type  = 27,
  },

  AmbulanceDuty = {
    Pos = { x = 339.43, y = -582.19, z = 27.88 },
    Size = { x = 2, y = 2, z = 1.5 },
    Color = { r = 0, g = 255, b = 0 },
    Type = 27,
  },

  AmbulanceDuty2 = {
    Pos = { x = 304.92, y = -600.21, z = 43.28 },
    Size = { x = 2, y = 2, z = 1.5 },
    Color = { r = 0, g = 255, b = 0 },
    Type = 27,
  },

  MechanicDuty = {
    Pos = { x = -1169.51, y = -1706.26, z = 3.67 },
    Size = { x = 2.5, y = 2.5, z = 1.5 },
    Color = { r = 0, g = 255, b = 0 },
    Type = 27,
  },

  TaxiDuty = {
    Pos = { x = 904.013, y = -166.41, z = 72.19 },
    Size = { x = 2.5, y = 2.5, z = 1.5 },
    Color = { r = 0, g = 255, b = 0 },
    Type = 27,
  },
}