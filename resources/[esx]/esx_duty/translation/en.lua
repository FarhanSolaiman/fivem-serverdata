Locales['en'] = {
  -- regulars
  	['duty'] = 'Press ~INPUT_CONTEXT~ to ~g~enter~s~/~r~exit~s~ duty',
	['onduty'] = 'You went onduty.',
	['offduty'] = 'You went offduty.',
	['notpol'] = 'You are not a policemen.',
	['notamb'] = 'You are not a hospital staff.',
	['notmec'] = 'You are not a mechanic.',
	['nottaxi'] = 'You are not from uber.'
}
