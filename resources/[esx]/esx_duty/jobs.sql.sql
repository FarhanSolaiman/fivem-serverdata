INSERT INTO `jobs` (name, label) VALUES
  ('offpolice','Off-Duty'),
  ('offambulance','Off-Duty'),
  ('offmechanic','Off-Duty')
;

INSERT INTO `job_grades` (job_name, grade, name, label, salary, skin_male, skin_female) VALUES
  ('offpolice',0,'recruit','Recruit',10000,'{}','{}'),
  ('offpolice',1,'officer','Officer',20000,'{}','{}'),
  ('offpolice',2,'sergeant','Sergeant',30000,'{}','{}'),
  ('offpolice',3,'lieutenant','Lieutenant',40000,'{}','{}'),
  ('offpolice',4,'boss','Boss',50000,'{}','{}'),
  ('offambulance',0,'ambulance','Ambulance',20,'{}','{}'),
  ('offambulance',1,'doctor','Doctor',40,'{}','{}'),
  ('offambulance',2,'chief_doctor','Chief Doctor',60,'{}','{}'),
  ('offambulance',3,'boss','Boss',80,'{}','{}'),
  ('offmechanic',0,'recrue','Recruit',12,'{}','{}'),
  ('offmechanic',1,'novice','Novice',24,'{}','{}'),
  ('offmechanic',2,'experimente','Experienced',36,'{}','{}'),
  ('offmechanic',3,'chief','Chief',48,'{}','{}'),
  ('offmechanic',4,'boss','Boss',0,'{}','{}'),
;