local Vehicles = {
    -- EMS
    {label = "Dodge Charger", spawnName = 'dodgeEMS'},
    {label = "Ford Explorer", spawnName = 'qrv'},
    -- Mechanic
    {label = "Ford F150", spawnName = 'orangef150'},
    {label = "Yamaha TMAX", spawnName = 'TMAX'},
    -- Police
    {label = "2014 Dodge Charger", spawnName = 'chargleg2'},
    {label = "2019 Dodge Charger", spawnName = 'chargleg4'},
    {label = "Ford Crown Victoria", spawnName = 'cvpileg'},
    {label = "Ford F250", spawnName = 'f250leg'},
    {label = "Ford Explorer", spawnName = 'fpiuleg2'},
    {label = "Dodge Ram", spawnName = 'ramleg'},
    {label = "GMC Sierra", spawnName = 'sierraleg'},
    {label = "Chevrolet Tahoe", spawnName = 'taholeg3'},
    {label = "Ford Taurus", spawnName = 'tarleg'},
    {label = "BMW R1200RT", spawnName = 'bikeleg'},
    {label = "Harley Davidson Electraglide", spawnName = 'bikeleg2'},
    {label = "McLaren P1", spawnName = 'polp1'},
    {label = "Ford GT", spawnName = 'polgt17'},
    {label = "Lamborghini Aventador", spawnName = 'polaventa'},
    -- Admin/Mod Vehicles
    {label = "Toyota AE86", spawnName = 'ae86'},
    {label = "Chevrolet Corvette C7R", spawnName = 'c7r'},
    {label = "BMW i8", spawnName = 'mi8'},
    {label = "Nissan GTR", spawnName = 'r35'},
    {label = "Lamborghini Terzo Millennio", spawnName = 'terzo'},
    {label = "Ford GT Mustang", spawnName = 'rmodmustang'},
    {label = "Toyota Camry 2018", spawnName = 'cam8tun'},
    {label = "Toyota '86 Stanced", spawnName = 'gt86panv2'},
    {label = "Ferrari 488 Pista Spider", spawnName = 'pistas'},
    {label = "Honda Civic Type-R", spawnName = 'fk8'},
    {label = "Dodge Challenger", spawnName = '16challenger'},
    {label = "Mitsubishi Lancer Evolution VI", spawnName = 'cp9a'},
    {label = "Toyota Supra", spawnName = 'rmodsupra'},
    {label = "Lexus RC-F GT3", spawnName = 'rcfgt3'},
    {label = "Mazda RX7", spawnName = 'rx7rb'},
    {label = "Ferrari Monza SP", spawnName = 'monza'},
    {label = "Chevrolet Camaro SS", spawnName = '16ss'},
    {label = "McLaren Senna", spawnName = 'senna'},
    --Modded Vehicles
    {label = "Chevrolet Silverado 1500 LTZ", spawnName = '1500dj'},
    {label = "BMW S1000 RR 2014", spawnName = 'bmws'},
    {label = "Honda Civic EK9", spawnName = 'ek9'},
    {label = "Ford F150 SVT Raptor R", spawnName = 'f150'},
    {label = "Honda CBR1000RR", spawnName = 'hcbr17'},
    {label = "Audi R8", spawnName = 'r8ppi'},
    {label = "Mitsubishi Lancer Evolution X", spawnName = 'lanex400'},
    {label = "Nissan GT-R R32", spawnName = 'r32'},
    {label = "Nissan Silvia S14 Kouki", spawnName = 'silvia3'},
    {label = "Subaru Impreza STI", spawnName = 'sti'}
}
    
Citizen.CreateThread(function()
    for num,vehicle in ipairs(Vehicles) do
        AddTextEntryByHash(GetHashKey(vehicle.spawnName), vehicle.label)
    end
end)