resource_manifest_version '44febabe-d386-4d18-afbe-5e627f4af937'
 
files {
    'cars/**/**/handling.meta',
    'cars/**/**contentunlocks.meta',
    'cars/**/vehiclelayouts.meta',    -- Not Required
    'cars/**/vehicles.meta',
    'cars/**/carcols.meta',
    'cars/**/carvariations.meta'
}

data_file 'HANDLING_FILE' 'cars/**/*handling.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'cars/**/*vehiclelayouts.meta'   -- Not Required
data_file 'CONTENT_UNLOCKING_META_FILE' 'cars/**/**contentunlocks.meta'
data_file 'VEHICLE_METADATA_FILE' 'cars/**/*vehicles.meta'
data_file 'CARCOLS_FILE' 'cars/**/*carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'cars/**/*carvariations.meta'